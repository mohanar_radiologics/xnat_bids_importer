# BIDS Data Importer #

This application traverses a folder which is organized as per BIDS (Brain Imaging Data Structure) protocol (Ref: http://bids.neuroimaging.io/). Data is imported into XNAT by creating:

Project

Subject(s)

Imaging Sessions(s)


# Launching BIDS Data Importer #

java -jar bids-dataimport-1.0.8-all.jar XNAT_HOST XNAT_USERNAME XNAT_PASSWORD PATH_TO_BIDS_DATA PATH_TO_LOG_FILE

The importer expects a file called dataset_description.json file which contains meta-data for the project. 

(the jar file bids-dataimport-1.0.8-all.jar can be downloaded from the Downloads section of this repository (link: https://bitbucket.org/mohanar_radiologics/xnat_bids_importer/downloads) 

# Building  "

gradlew clean shadowJar


