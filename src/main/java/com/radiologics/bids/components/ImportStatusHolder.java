package com.radiologics.bids.components;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.radiologics.bids.components.elements.ProjectImportStatus;

@Component
public class ImportStatusHolder {
	
	public enum SyncType { PROJECT_SYNC, EXPERIMENT_SYNC, NONE_SINCE_STARTUP }; 
	private final Map<String,ProjectImportStatus> statusMap = new HashMap<>();
	
	public ProjectImportStatus getProjectImportStatus(String projectId) {
		if (!statusMap.containsKey(projectId)) {
			statusMap.put(projectId, new ProjectImportStatus());
		}
		return statusMap.get(projectId);
		
	}

}
