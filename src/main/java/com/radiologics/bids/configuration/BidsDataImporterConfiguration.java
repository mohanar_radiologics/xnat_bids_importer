package com.radiologics.bids.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Mohana Ramaratnam
 *
 */
	@Configuration
	
	@ComponentScan(basePackages = {
			"com.radiologics.bids.connection",
			"com.radiologics.bids.manifest",
			"com.radiologics.bids.manifest.impl",
			"com.radiologics.bids.services",
			"com.radiologics.bids.components",
			"com.radiologics.bids.importer"
			})
	public class BidsDataImporterConfiguration {

	}
