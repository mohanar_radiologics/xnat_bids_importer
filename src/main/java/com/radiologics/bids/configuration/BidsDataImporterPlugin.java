package com.radiologics.bids.configuration;

import org.nrg.framework.annotations.XnatPlugin;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Mohana Ramaratnam
 *
 */

@XnatPlugin(value = "bidsImporterPlugin", name = "BIDS Data Importer Plugin")
@ComponentScan({
	"com.radiologics.bids.services",
	"com.radiologics.bids.components",
	"com.radiologics.bids.connection",
	"com.radiologics.bids.manifest"})

public class BidsDataImporterPlugin {
	

	public static Logger logger = Logger.getLogger(BidsDataImporterPlugin.class);

	public BidsDataImporterPlugin() {
		logger.info("Configuring BIDS Data Importer plugin");
	}


}
