package com.radiologics.bids.connection;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

/**
 * The Class XsyncResponseErrorHandler.
 * 
 * @author Mike Hodge
 */
public class RadiologicsResponseErrorHandler implements ResponseErrorHandler {

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		final HttpStatus statusCode = response.getStatusCode();
		return (statusCode.is4xxClientError() || statusCode.is5xxServerError());
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		final HttpStatus statusCode = response.getStatusCode();
		switch (statusCode.series()) {
			case CLIENT_ERROR:
				if (statusCode.equals(HttpStatus.UNAUTHORIZED)) {
						throw new RadiologicsHttpAuthenticationException(statusCode, response.getStatusText());
				}
				throw new HttpClientErrorException(statusCode, response.getStatusText());
			case SERVER_ERROR:
				throw new HttpServerErrorException(statusCode, response.getStatusText());
			default:
				throw new RestClientException("Unknown status code [" + statusCode + "]");
		}
	}
	
}
