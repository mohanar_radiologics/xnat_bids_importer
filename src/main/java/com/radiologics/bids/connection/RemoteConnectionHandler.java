package com.radiologics.bids.connection;

import java.util.Hashtable;

/**
 * @author Mohana Ramaratnam
 *
 */
public class RemoteConnectionHandler {
	
	    private static  RemoteConnectionHandler instance;
	    private static  String _host;
	    private static  String _user;
	    private static  String _password;
	    
	    
	    private RemoteConnectionHandler(String host, String user, String password){
	    	_host = host;
	    	_user = user;
	    	_password = password;
	    	
	    }

	    public static RemoteConnectionHandler GetInstance(String host, String user, String password){
	        if (instance == null) {
	        	instance = new RemoteConnectionHandler(host, user, password);
	        }
	        return instance;
	    }
	    
	    public RemoteConnection getRemoteConnection() {
			RemoteConnection conn = new RemoteConnection();
			conn.setUrl(_host);
			conn.setUsername(_user);
			conn.setPassword(_password);
			return conn;
	    }
}
