package com.radiologics.bids.connection;

import java.io.File;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Maps;
import com.radiologics.bids.services.ImportManifestService;
import com.radiologics.bids.services.RemoteRESTService;

/**
 * The Class RemoteConnectionManager.
 *
 * @author Mohana Ramaratnam
 */
@Service
public class RemoteConnectionManager {

	/** The logger. */
	private static Logger logger = Logger.getLogger(RemoteConnectionManager.class);

	/** The remote rest service. */
	private final RemoteRESTService     _remoteRESTService;
	
	private final ImportManifestService   _importManifestService;

	/** The Constant cache. */
	private static final Map<String,String> sessionCache = Maps.newHashMap();

	@Autowired
	public RemoteConnectionManager(final RemoteRESTService remoteRESTService, final ImportManifestService importManifestService) {
		_remoteRESTService = remoteRESTService;
		_importManifestService = importManifestService;
	}

	/**
	 * Gets the auth headers.
	 *
	 * @param connection the connection
	 * @param useJSESSIONID the use jsessionid
	 * @param refreshCache the refresh cache
	 * @return the auth headers
	 */
	public static HttpHeaders GetAuthHeaders(RemoteConnection connection, boolean useJSESSIONID, boolean refreshCache){
		final HttpHeaders headers = new HttpHeaders();
		if (useJSESSIONID) {
			final String JSESSIONID = getJsessionId(connection);
			if (JSESSIONID!=null && JSESSIONID.length()>0) {
				headers.add(HttpHeaders.COOKIE, "JSESSIONID=" + JSESSIONID);
				return headers;
			}
		} 
		if (refreshCache) {
			getJsessionId(connection, true);
		}
		headers.add("Authorization", "Basic " + getBase64Credentials(connection));
		return headers;
	}

	/**
	 * Gets the auth headers.
	 *
	 * @param connection the connection
	 * @param useJSESSIONID the use jsessionid
	 * @return the http headers
	 */
	public static HttpHeaders GetAuthHeaders(RemoteConnection connection, boolean useJSESSIONID) {
		// We won't refresh the JSESSSIONID cache unless we're told to
		return GetAuthHeaders(connection, useJSESSIONID, false);
	}
	
	/**
	 * Gets the auth headers (sends credentials.  does not use the cache version).
	 *
	 * @param connection the connection
	 * @return the http headers
	 */
	public static HttpHeaders GetAuthHeaders(RemoteConnection connection) {
		// For safety, let's not use JSESSIONID unless we're told to. 
		return GetAuthHeaders(connection, false, false);
	}

	/**
	 * Gets the jsession id (option to refresh the cache).
	 *
	 * @param connection the connection
	 * @param refreshCache refresh the cache, or used cached version?
	 * @return the jsession id
	 */
	private static String getJsessionId(RemoteConnection connection, boolean refreshCache) {
		final String cacheKey = connectionToCacheKey(connection);
		if (!refreshCache && sessionCache.containsKey(cacheKey)) {
			return sessionCache.get(cacheKey);
		}
		final HttpEntity<?> httpEntity = new HttpEntity<Object>(GetAuthHeaders(connection, false));
		final SimpleClientHttpRequestFactory requestFactory =new SimpleClientHttpRequestFactory();
		final RestTemplate restTemplate = new RestTemplate(requestFactory);
		final ResponseEntity<String> response = restTemplate.exchange(connection.getUrl() + "/data/JSESSIONID", HttpMethod.GET, httpEntity, String.class);
		if (response.getStatusCode().equals(HttpStatus.OK)) {
			final String responseBody = response.getBody();
			if (responseBody!=null && responseBody.length()>0) {
				sessionCache.put(cacheKey, responseBody);
			}
			return response.getBody();
		}
		return null;
	}
	
	/**
	 * Gets the jsession id (uses cached version if available (does not refresh the cache)).
	 *
	 * @param connection the connection
	 * @return the jsession id
	 */
	private static String getJsessionId(RemoteConnection connection) {
		return getJsessionId(connection, false);
	}

	/**
	 * Connection to cache key.
	 *
	 * @param connection the connection
	 * @return the string
	 */
	private static String connectionToCacheKey(RemoteConnection connection) {
		return connection.getUrl();
	}

	/**
	 * Gets the base64 credentials.
	 *
	 * @param conn the conn
	 * @return the base64 credentials
	 */
	private static String getBase64Credentials(RemoteConnection conn) {
		final String plainCreds;
		plainCreds = conn.getUsername()+":"+conn.getPassword();
		final byte[] plainCredsBytes = plainCreds.getBytes();
		final byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		final String base64Creds = new String(base64CredsBytes);
		return base64Creds;
	}


	/**
	 * Gets the remote REST service.
	 * @return The remote REST service.
     */
	public RemoteRESTService getRemoteRESTService() {
		return _remoteRESTService;
	}

	public ImportManifestService getImportManifestService() {
		return _importManifestService;
	}

	/**
	 * Import subject.
	 *
	 * @param connection the connection
	 * @param subject the subject
	 * @return the remote connection response
	 * @throws Exception the exception
	 */
	public RemoteConnectionResponse importSubject(RemoteConnection connection, XnatSubjectdataBean subject) throws Exception {
		return _remoteRESTService.importSubject(connection, subject);
	}

	/**
	 * Import project.
	 *
	 * @param connection the connection
	 * @param project the project
	 * @return the remote connection response
	 * @throws Exception the exception
	 */
	public RemoteConnectionResponse importProject(RemoteConnection connection, XnatProjectdataBean project) throws Exception {
		return _remoteRESTService.importProject(connection, project);
	}

	
	/**
	 * Import project resource.
	 *
	 * @param connection the connection
	 * @param projectId the project id
	 * @param resourceLabel the resource label
	 * @param zipFile the zip file
	 * @return the remote connection response
	 * @throws Exception the exception
	 */
	public RemoteConnectionResponse importProjectResource(RemoteConnection connection, String projectId, String resourceLabel, File zipFile) throws Exception {
		return _remoteRESTService.importProjectResource(connection, projectId, resourceLabel, zipFile);
	}


	/**
	 * Import Resource.
	 *
	 * @param connection the connection
	 * @param ulr_postfix the well formed URL after host
	 * @param resourceLabel the resource label
	 * @param zipFile the zip file
	 * @return the remote connection response
	 * @throws Exception the exception
	 */
	public RemoteConnectionResponse importResource(RemoteConnection connection, String url_postfix, File zipFile) throws Exception{
		return _remoteRESTService.importResource(connection, url_postfix, zipFile);
	}

	/**
	 * Import MRSession resource.
	 *
	 * @param connection the connection
	 * @param experiment the experiment
	 * @param resourceLabel the resource label
	 * @param zipFile the zip file
	 * @return the remote connection response
	 * @throws Exception the exception
	 */
	public RemoteConnectionResponse importImageSession(RemoteConnection connection, XnatImagesessiondataBean exp) throws Exception{
		return _remoteRESTService.importImageSession(connection, exp);
	}
	

	/**
	 * Gets the result.
	 *
	 * @param connection the connection
	 * @param uri the uri
	 * @return the result
	 * @throws Exception the exception
	 */
	public RemoteConnectionResponse getResult(RemoteConnection connection,String uri) throws Exception{
		return _remoteRESTService.getResult(connection, uri);
	}
}
