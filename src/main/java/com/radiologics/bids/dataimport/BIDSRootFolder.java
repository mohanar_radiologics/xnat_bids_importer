package com.radiologics.bids.dataimport;

import java.text.SimpleDateFormat;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.xdat.bean.XnatProjectdataBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.radiologics.bids.components.ImportStatusHolder.SyncType;
import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.connection.RemoteConnectionResponse;
import com.radiologics.bids.dataimport.manager.ImportManager;
import com.radiologics.bids.manifest.ProjectImportedItem;
import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.services.ImportStatusService;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.bids.utils.BIDSFileUtils;
/**
 * @author Mohana Ramaratnam
 *
 */
public class BIDSRootFolder {
	    
	    private final RemoteConnectionManager    _manager;
	    private 	  ImportObserver			 _observer;
	    private final RemoteConnection           _connection;
		private final ImportStatusService _syncStatusService;
	    private static final Logger _log = LoggerFactory.getLogger(BIDSRootFolder.class);

	   private File _participantTSVFile = null;
	   private File _participantJSONFile = null;
	   private File _dataSetDescriptorJson = null;
	   private String _rootFolderPath;
	
	
	public BIDSRootFolder(final RemoteConnectionManager manager,ImportStatusService syncStatusService,RemoteConnection connection, String rootFolderPath) {
		_manager = manager;
		_syncStatusService = syncStatusService;
		_connection = connection;
		_rootFolderPath = rootFolderPath;
		
	}
	
	public void importData() {
		//Does the root folder exist?
		//If it does, parse the subfolders

		File bidsRootDir = new File(_rootFolderPath);
		if (bidsRootDir.exists()) {
		    if (!bidsRootDir.isDirectory()) {
		        System.out.println("Incorrect Path - appears that the directory " + _rootFolderPath + " does not exist");
		        System.exit(1);
		    }
		}
		//The root folder can have many subfolders. Each subfolder would be a project data or it could be a single dataset
		File[] subDirectories = null;
		if (BIDSFileUtils.representsBIDSDataset(bidsRootDir)) {
			subDirectories = new File[1];
			subDirectories[0] = bidsRootDir;
		}else {
			subDirectories = bidsRootDir.listFiles(new FilenameFilter() {
			    public boolean accept(File dir, String name) {
			    	File subDir = new File(dir, name);
			    	boolean acceptMe = false;
			    	if (subDir.isDirectory()) {
			    		acceptMe = BIDSFileUtils.representsBIDSDataset(subDir); 
			    	}
			    	return acceptMe;
			    }
			});
		}
		

		if (subDirectories !=null && subDirectories.length > 0) {
			for (File s: subDirectories) {
				//This will be the project id
				String _projectId = s.getName().trim().replaceAll(" ", "_");
				if (_projectId.length() > 24) {
					//XNAT Project Titles are only 24 characters long
					_projectId=_projectId.substring(0,9);
					Date now = new Date();
					String pattern = "yyyddMMHHmmss";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
					String date = simpleDateFormat.format(now);
					_projectId += "_"+date;
				}
				System.out.println("Importing to Project " + _projectId);
				try {
			        ImportManager.BEGIN_SYNC(_manager.getImportManifestService(), _projectId);
			        _syncStatusService.registerSyncStart(_projectId,SyncType.PROJECT_SYNC,ImportManager.getProjectManifest(_projectId));
					_observer  = new ImportObserver(_projectId);
					
	
					File[] jsonFiles = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        return name.equals(BIDSConstants.DATASET_DESCRIPTION_JSON);
					    }
					});
					
					if (jsonFiles != null && jsonFiles.length == 1) {
						_dataSetDescriptorJson = jsonFiles[0];
					}
					
					File readmeFile = null;
					File changesFile = null;
					File[] readmeFiles = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        return name.equals(BIDSConstants.README);
					    }
					});
					
					if (readmeFiles != null && readmeFiles.length == 1) {
						readmeFile = readmeFiles[0];
					}
					File[] changesFiles = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        return name.equals(BIDSConstants.CHANGES);
					    }
					});
					
					if (changesFiles != null && changesFiles.length == 1) {
						changesFile = changesFiles[0];
					}
					
					
					//Extract Participant TSV file
					
					File[] files = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        return name.equals(BIDSConstants.PARTICIPANTS_TSV_FILENAME);
					    }
					});
					
					if (files != null && files.length == 1) {
						_participantTSVFile = files[0];
					}
	
					files = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        return name.equals(BIDSConstants.PARTICIPANTS_JSON_FILENAME);
					    }
					});
					
					if (files != null && files.length == 1) {
						_participantJSONFile = files[0];
					}
					
					ImportProject projectImporter = new ImportProject(_manager, _projectId, _dataSetDescriptorJson);
					
					if (_participantTSVFile != null) {
						projectImporter.setParticipantTSV(_participantTSVFile);
					}
					if (_participantJSONFile != null) {
						projectImporter.setParticipantJson(_participantJSONFile);
					}
					
					if (readmeFiles != null) {
						projectImporter.addResource(readmeFile);
					}
					if (changesFile != null) {
						projectImporter.addResource(changesFile);
					}
	
					
					//Extract JSON Files
					
					jsonFiles = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        return (!name.equals(BIDSConstants.DATASET_DESCRIPTION_JSON) && !name.equals(BIDSConstants.PARTICIPANTS_JSON_FILENAME) && name.endsWith(".json"));
					    }
					});
					
					if (jsonFiles != null && jsonFiles.length > 0) {
						for (File f: jsonFiles) {
							if (f != null)
							 projectImporter.addResource(f);
						}
					}
					
					//Extract Subject Folders
					
					File[] subFolders = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        boolean nameMatches =  name.startsWith(BIDSConstants.SUBJECT_PREFIX);
					        File f = new File(dir,name);
					        boolean isFolder = f.isDirectory();
					        return (nameMatches && isFolder);
					    }
					});

					File[] otherFolders = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        boolean nameMatches =  !name.startsWith(BIDSConstants.SUBJECT_PREFIX) && !name.equals(BIDSConstants.DERIVATIVES_FOLDER_NAME);
					        File f = new File(dir,name);
					        boolean isFolder  = f.isDirectory();
					        return (nameMatches && isFolder);
					    }
					});
					
					File[] derivativeFolders = s.listFiles(new FilenameFilter() {
					    public boolean accept(File dir, String name) {
					        boolean nameMatches =  name.equals(BIDSConstants.DERIVATIVES_FOLDER_NAME);
					        File f = new File(dir,name);
					        boolean isFolder  = f.isDirectory();
					        return (nameMatches && isFolder);
					    }
					});
					if (derivativeFolders != null && derivativeFolders.length > 0) {
						File derivative = derivativeFolders[0];
						projectImporter.setDerivativesFolder(derivative);
					}
					
					projectImporter.setSubjectFolders(Arrays.asList(subFolders));
					projectImporter.setOtherFolders(Arrays.asList(otherFolders));
					
					RemoteConnectionResponse response = projectImporter.save(_connection, _observer,s);
					ProjectImportedItem projectImported = new ProjectImportedItem(_projectId, _projectId);
					projectImported.setXsiType(XnatProjectdataBean.SCHEMA_ELEMENT_NAME);
					projectImported.setSyncTime(new Date());
					projectImported.addObserver(_observer);
					if (response.wasSuccessful()) {
						projectImported.setSyncStatus("IMPORTED ");
					}else {
						projectImported.setSyncStatus("IMPORT_FAILED");
						projectImported.setMessage("See XML file at log file path");
					}
					projectImported.stateChanged();

					//Create subjects from the participant.tsv file
		            ImportManager.END_SYNC( _projectId,  true);
		            _syncStatusService.registerSyncEnd(_projectId,ImportManager.getProjectManifest(_projectId));
				}catch(Exception e) {
					e.printStackTrace();
		            //Roll back the syncBlocked flag
		            _log.debug(e.getLocalizedMessage());
					ProjectImportedItem projectImported = new ProjectImportedItem(_projectId, _projectId);
					projectImported.setXsiType(XnatProjectdataBean.SCHEMA_ELEMENT_NAME);
					projectImported.setSyncTime(new Date());
					projectImported.addObserver(_observer);
					projectImported.setSyncStatus("IMPORT_FAILED");
					projectImported.setMessage("See XML file at log file path " + e.getMessage());
					projectImported.stateChanged();
		            _syncStatusService.registerSyncEnd(_projectId,ImportManager.getProjectManifest(_projectId));
				}finally {
					_observer.close();
				}

			}
		}else {
	        System.out.println("Incorrect Path - appears that the directory " + _rootFolderPath + " does not represent a BIDS Dataset. Missing dataset_description.json file ");
	        System.exit(1);
		}
	}

}
