package com.radiologics.bids.dataimport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import org.nrg.xdat.bean.XnatSubjectdataBean;

import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.connection.RemoteConnectionResponse;
import com.radiologics.bids.dataimport.manager.ImportManager;
import com.radiologics.bids.manifest.SubjectImportItem;
import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ImportParticipants {
	
    private final RemoteConnectionManager    _manager;
	File _participantTSVFile;
	String[] columnHeaders ;
	RemoteConnection _connection;
	
	public ImportParticipants(RemoteConnectionManager manager, RemoteConnection connection, File participantTSVFile) {
		_manager = manager;
		_connection = connection;
		_participantTSVFile = participantTSVFile;
	}

	public void uploadParticipants(ImportObserver observer, String projectId) {
		if (_participantTSVFile != null && _participantTSVFile.exists() && _participantTSVFile.isFile()) {
			try {
				BufferedReader bReader = new BufferedReader(new FileReader(_participantTSVFile));
		        String line;
		        /**
		         * Looping the read block until all lines in the file are read.
		         */
		        int firstLine = 0;
		        while ((line = bReader.readLine()) != null) {
		            /**
		             * Splitting the content of tabbed separated line
		             */
		            String datavalue[] = line.split("\\t");
		            firstLine++;
		            if (firstLine == 1) {
		            	if (datavalue != null && datavalue.length > 0) {
		            		columnHeaders=datavalue;
		            	}
		            	boolean validColumns = false;
		            	for(String col:columnHeaders) {
		            		if (col.equals(BIDSConstants.PARTICIPANT_ID)) {
		            			validColumns = true;
		            			break;
		            		}
		            	}
		            	if (!validColumns) {
		            		break;
		            	}
		            }else {
		            	if (datavalue != null && datavalue.length > 0) {
		                	Properties prop = new Properties();
		                	for (int i=0; i<datavalue.length; i++) {
		                		prop.setProperty(columnHeaders[i], datavalue[i]);
		                	}
		                	ParticipantRow participant = new ParticipantRow(prop);
		                	XnatSubjectdataBean subject = participant.toSubjectBean();
		                	subject.setProject(projectId);
		                	RemoteConnectionResponse response = _manager.importSubject(_connection, subject);
		                	if (!response.wasSuccessful()) {
		                		ImportManager.SAVE_FAILED_UPLOAD(subject);
		                	}else {
		                		subject.setId(response.getResponseBody());
		                		//ImportManager.SET_SUBJECT_XNAT_ID(projectId, subject.getLabel(), subject.getId());
		                		ImportManager.SAVE_SUBJECT_XNAT_ID(projectId, subject.getLabel(), subject.getId());
		    					SubjectImportItem subjectImported = new SubjectImportItem(subject.getId(), subject.getLabel());
		    					subjectImported.setXsiType(XnatSubjectdataBean.SCHEMA_ELEMENT_NAME);
		    					subjectImported.setSyncTime(new Date());
		    					subjectImported.addObserver(observer);
		    					if (response.wasSuccessful()) {
		    						subjectImported.setSyncStatus("IMPORTED ");
		    					}else {
		    						subjectImported.setSyncStatus("IMPORT_FAILED");
		    						subjectImported.setMessage("See XML file at log file path");
		    					}
		    					subjectImported.stateChanged();
		                	}
		            	}
		            }
		        }
		        bReader.close();
			}catch(FileNotFoundException fne) {
				System.out.println("File Not Found " + _participantTSVFile.getAbsolutePath());
				System.exit(1);
			}catch(IOException ioe) {
				System.out.println("IO Exception while reading file  " + _participantTSVFile.getAbsolutePath());
				System.exit(1);
			}catch(Exception e) {
				
			}
		}
	}
}
