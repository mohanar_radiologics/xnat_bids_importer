package com.radiologics.bids.dataimport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.nrg.xdat.bean.XnatDatatypeprotocolBean;
import org.nrg.xdat.bean.XnatFielddefinitiongroupBean;
import org.nrg.xdat.bean.XnatFielddefinitiongroupFieldBean;
import org.nrg.xdat.bean.XnatFielddefinitiongroupFieldPossiblevalueBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.connection.RemoteConnectionResponse;
import com.radiologics.bids.dataimport.ImportScans.NiftiFileFilter;
import com.radiologics.bids.dataimport.manager.ImportManager;
import com.radiologics.bids.manifest.SubjectImportItem;
import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.bids.utils.FileZipUtils;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ImportProject {
	
    private final RemoteConnectionManager    _manager;
	String _projectId;
	File _dataDescriptionJSON;
	File _participantTSV;
	File _participantJson;
	List<File> projectResources ;
	List<File> subjectFolders ;
	File _derivatesFolder;
	List<Properties> participantProperties = null;
	boolean participantTSVHasAge = false;
	
	public ImportProject(final RemoteConnectionManager manager,String projectId, File dataDescriptionJSON) {
		_manager = manager;
		_projectId = projectId;
		_dataDescriptionJSON = dataDescriptionJSON;
		projectResources = new ArrayList<File>();
		subjectFolders = new ArrayList<File>();
		_derivatesFolder = null;
	}
	
	public void addResource(File rscFile) {
		if (rscFile != null) projectResources.add(rscFile);
	}
	public void addSubjectFolder(File s) {
		subjectFolders.add(s);
	}
	public void setSubjectFolders(List<File> sFs) {
		subjectFolders.addAll(sFs);
	}
	public void addOtherFolder(File s) {
		if (s!=null) projectResources.add(s);
	}
	public void setOtherFolders(List<File> sFs) {
		if (sFs != null && sFs.size() > 0) projectResources.addAll(sFs);
	}

	public void setDerivativesFolder(File derivative) {
		this._derivatesFolder = derivative;
	}

	public File getDerivativesFolder() {
		return _derivatesFolder ;
	}

	
	public void setParticipantTSV(File participantTSVFile) {
		_participantTSV = participantTSVFile;
		if (_participantTSV != null) projectResources.add(_participantTSV);
	}

	public void setParticipantJson(File participantJsonFile) {
		_participantJson = participantJsonFile;
		if (_participantJson != null) projectResources.add(_participantJson);
	}
	
	public RemoteConnectionResponse save(RemoteConnection connection, ImportObserver observer, File rootDir) throws Exception{
		XnatProjectdataBean project = new XnatProjectdataBean();
		project.setId(_projectId);
		project.setSecondaryId(_projectId);
		XnatDatatypeprotocolBean dataTypeProtocol = new XnatDatatypeprotocolBean();
		dataTypeProtocol.setId(_projectId+"_xnat_mrSessionData");
		dataTypeProtocol.setName("MR Sessions");
		dataTypeProtocol.setDataType("xnat:mrSessionData");
		XnatFielddefinitiongroupBean defaultDefinition = new XnatFielddefinitiongroupBean();
		defaultDefinition.setId("default");
		defaultDefinition.setDataType("xnat:mrSessionData");
		defaultDefinition.setProjectSpecific("0");
		dataTypeProtocol.addDefinitions_definition(defaultDefinition);
		project.addStudyprotocol(dataTypeProtocol);
		//Subject Study Protocol
		dataTypeProtocol = new XnatDatatypeprotocolBean();
		dataTypeProtocol.setId(_projectId+"_xnat_subjectData");
		dataTypeProtocol.setName("Subjects");
		dataTypeProtocol.setDataType("xnat:subjectData");
		defaultDefinition = new XnatFielddefinitiongroupBean();
		defaultDefinition.setId("default");
		defaultDefinition.setDataType("xnat:subjectData");
		defaultDefinition.setProjectSpecific("0");
		dataTypeProtocol.addDefinitions_definition(defaultDefinition);

		if (_dataDescriptionJSON != null && _dataDescriptionJSON.exists()) {
			//Parse to populate the fields of the project
			project.setName(_projectId);
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				JsonNode rootNode = objectMapper.readTree(_dataDescriptionJSON);
				String desc="";

				JsonNode name = rootNode.get("Name");
				if (name != null) {
					//project.setName(name.asText());
				}
				for (String key: BIDSConstants.DataDescriptionKeys) {
					JsonNode node = rootNode.get(key);
					if (node != null) {
						   desc += key + "="+ node.toString() + "\n";					}
				}
				if (!desc.equals("")) {
					project.setDescription(desc);
				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

		//Create subject custom variable set
		if (_participantJson != null) {
			XnatFielddefinitiongroupBean bidsDefinition = new XnatFielddefinitiongroupBean();
			bidsDefinition.setId("BIDS_"+project.getId());
			bidsDefinition.setDescription("BIDS Subject Fields from participants.json file");
			bidsDefinition.setDataType("xnat:subjectData");
			bidsDefinition.setProjectSpecific("1");
			
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				JsonNode rootNode = objectMapper.readTree(_participantJson);
				if (rootNode != null) {
					Iterator<String> fields = rootNode.fieldNames();
					while(fields.hasNext()) {
						String f = fields.next();
						JsonNode node = rootNode.get(f);
						if (node != null) {
							XnatFielddefinitiongroupFieldBean singleField = new XnatFielddefinitiongroupFieldBean();
							singleField.setName(f);
							singleField.setType("custom");
							singleField.setDatatype("string");
							singleField.setRequired("0");
							singleField.setXmlpath("xnat:subjectData/fields/field[name="+f+"]/field");
							JsonNode levelsNode = node.get("Levels");
							if (levelsNode != null) {
								Iterator<String> levelsFields = levelsNode.fieldNames();
								while(levelsFields.hasNext()) {
									String possibleValue = levelsFields.next();
									XnatFielddefinitiongroupFieldPossiblevalueBean  pValueBean = new XnatFielddefinitiongroupFieldPossiblevalueBean();
									pValueBean.setPossiblevalue(possibleValue);
									singleField.addPossiblevalues_possiblevalue(pValueBean);
								}								
							}
							bidsDefinition.addFields_field(singleField);
						}
						
					}
				}
				dataTypeProtocol.addDefinitions_definition(bidsDefinition);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		project.addStudyprotocol(dataTypeProtocol);
		//Upload the project XML
		try {
			 RemoteConnectionResponse response = _manager.importProject(connection, project);
			 if (!response.wasSuccessful()) {
				 ImportManager.SAVE_FAILED_UPLOAD(project);
			 }else {
				 //Now upload all the resource files
				 //Create a zip file of resources and use import
				 String remoteProjectId = response.getResponseBody();
				 projectResources.add(_dataDescriptionJSON);
				 if (_derivatesFolder != null && _derivatesFolder.isDirectory()) {
					 File[] subFoldersInDerivatives = _derivatesFolder.listFiles();
					 for (File s: subFoldersInDerivatives) {
						 if (s.exists() && s.isDirectory() && !s.getName().startsWith(BIDSConstants.SUBJECT_PREFIX)) {
							 projectResources.add(s);
						 }
					 }
				 }
				 FileZipUtils fileZipUtils = new FileZipUtils();
				 File zipFile = fileZipUtils.buildZip(_projectId, projectResources, rootDir);
				 if (zipFile != null && zipFile.exists())
					 response = _manager.importProjectResource(connection, project.getId(), "BIDS", zipFile);
			 }
			 if (!response.wasSuccessful()) {
				 ImportManager.SAVE_FAILED_UPLOAD(project);
			 }
			 if (_participantTSV != null) {
				 //Now create the subjects
				 participantTSVToProperties();
				 ImportParticipants importParticipants = new ImportParticipants(_manager, connection,_participantTSV);
				 importParticipants.uploadParticipants(observer,project.getId());
			 }else {
				 //Create almost empty subject
				 for (File subjectFolder: subjectFolders) {
					 if (subjectFolder.exists() && subjectFolder.isDirectory()) {
						 String subjectName = subjectFolder.getName();
						 String subjectLabel = extractSubStrAfter(subjectName, BIDSConstants.SUBJECT_PREFIX);
						 if (subjectLabel != null) {
							 XnatSubjectdataBean subject = new XnatSubjectdataBean();
							 subject.setLabel(subjectLabel);
							 subject.setProject(_projectId);
							 System.out.println("Importing Subject: " + subjectLabel +  " from " + subjectName);
			                 response = _manager.importSubject(connection, subject);
			                	if (!response.wasSuccessful()) {
			                		ImportManager.SAVE_FAILED_UPLOAD(subject);
			                	}else {
			                		subject.setId(response.getResponseBody());
			                		ImportManager.SAVE_SUBJECT_XNAT_ID(_projectId, subject.getLabel(), subject.getId());
			    					SubjectImportItem subjectImported = new SubjectImportItem(subject.getId(), subject.getLabel());
			    					subjectImported.setXsiType(XnatSubjectdataBean.SCHEMA_ELEMENT_NAME);
			    					subjectImported.setSyncTime(new Date());
			    					subjectImported.addObserver(observer);
			    					if (response.wasSuccessful()) {
			    						subjectImported.setSyncStatus("IMPORTED ");
			    					}else {
			    						subjectImported.setSyncStatus("IMPORT_FAILED");
			    						subjectImported.setMessage("See XML file at log file path");
			    					}
			    					subjectImported.stateChanged();
			                	}
						 }
					 }
				 }
			 }
			 //Upload Experiment Files
			 for (File subjectFolder: subjectFolders) {
				 if (subjectFolder.exists() && subjectFolder.isDirectory()) {
					 String subjectFolderName = subjectFolder.getName();
					 String subjectLabel = extractSubStrAfter(subjectFolderName, BIDSConstants.SUBJECT_PREFIX);
					 String subjectAccessionId = ImportManager.GET_SUBJECT_XNAT_ID(_projectId, subjectLabel);
					 //Not saved so far via TSV File
					 if (subjectAccessionId == null) {
						 subjectAccessionId = subjectLabel;
					 }
					 File subjectSessionsTSVFile = new File(subjectFolder.getAbsolutePath(),subjectFolderName+"_sessions.tsv" );
					 if (subjectSessionsTSVFile != null && subjectSessionsTSVFile.exists() && subjectSessionsTSVFile.isFile()) {
						 ImportSubjectSessions subjectSessions = new ImportSubjectSessions(_manager,connection,subjectSessionsTSVFile, rootDir);
						 subjectSessions.uploadSession(observer, _projectId, subjectLabel);
					 }else {
						 //Either there are multiple-sessions or a Single Session with no TSV file
						 File[] sessionFolders = subjectFolder.listFiles(new FilenameFilter() {
						    public boolean accept(File dir, String name) {
						    	File subDir = new File(dir, name);
						    	boolean acceptMe = false;
						    	if (subDir.isDirectory() && name.startsWith(BIDSConstants.SESSION_PREFIX)) {
						    		acceptMe = true;
						    	}
						    	return acceptMe;
						    }
						});
						if (sessionFolders != null && sessionFolders.length > 0) {
							for (File mrFolder:sessionFolders) {
								String sessionFolderName = mrFolder.getName();
								String mrLabel = subjectLabel + "_MR" + extractSubStrAfter(sessionFolderName, BIDSConstants.SESSION_PREFIX);
								XnatMrsessiondataBean mr = new XnatMrsessiondataBean();
								mr.setLabel(mrLabel);
								mr.setVisitId(sessionFolderName);
								mr.setProject(_projectId);
								mr.setSubjectId(subjectAccessionId);
								if (participantTSVHasAge) {
									//Include the subject age at the time of experiment
									String age = getSubjectAgeAtExperiment(subjectLabel);
									if (null != age) {
										mr.setAge(age);
									}
								}
								ImportScans scanExtractor = new ImportScans(mrFolder, subjectLabel,mrLabel, _derivatesFolder, projectResources);
								Hashtable<XnatMrscandataBean, Hashtable<String,List<File>>> scans = scanExtractor.getScans();
								importMrSession(connection, mr, mrFolder,scans);	
							}
						}else {
							String mrLabel = subjectLabel + "_MR1";
							XnatMrsessiondataBean mr = new XnatMrsessiondataBean();
							mr.setLabel(mrLabel);
							mr.setVisitId(subjectFolderName);
							mr.setProject(_projectId);
							mr.setSubjectId(subjectAccessionId);
							if (participantTSVHasAge) {
								//Include the subject age at the time of experiment
								String age = getSubjectAgeAtExperiment(subjectLabel);
								if (null != age) {
									mr.setAge(age);
								}
							}
							ImportScans scanExtractor = new ImportScans(subjectFolder,subjectLabel,mrLabel, _derivatesFolder,projectResources);
							Hashtable<XnatMrscandataBean, Hashtable<String,List<File>>> scans = scanExtractor.getScans();
							importMrSession(connection, mr, subjectFolder,scans);
						}
						 
					 }
				 }
			 }
			 return response;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			 ImportManager.SAVE_FAILED_UPLOAD(project);
			 throw e;
		}
		
	}
	


	private void importMrSession(RemoteConnection connection,XnatMrsessiondataBean mr, File baseDir, Hashtable<XnatMrscandataBean, Hashtable<String,List<File>>> scans) throws Exception {
		Enumeration<XnatMrscandataBean> scanEnumerator = scans.keys();
		while (scanEnumerator.hasMoreElements()) {
			XnatMrscandataBean scan = scanEnumerator.nextElement();
			scan.setImageSessionId(mr.getLabel());
			mr.addScans_scan(scan);
		}

		try {
			RemoteConnectionResponse response = _manager.importImageSession(connection, mr);
			 if (!response.wasSuccessful()) {
				 ImportManager.SAVE_FAILED_UPLOAD(mr);
			 }else {
				 //Now upload all the scan resource files for each scan
				 //Create a zip file of resources and use import
				 String mrAccessionId = response.getResponseBody();
	     		 ImportManager.SAVE_EXPERIMENT_XNAT_ID(_projectId, mr.getLabel(), mrAccessionId);
	     		 scanEnumerator = scans.keys();
	     		 while (scanEnumerator.hasMoreElements()) {
	    			XnatMrscandataBean scan = scanEnumerator.nextElement();
	    			Hashtable<String,List<File>> scanFiles = scans.get(scan);
	    			if (scanFiles != null && scanFiles.size() > 0 ) {
	    	     		 FileZipUtils fileZipUtils = new FileZipUtils();
	    				 Hashtable<String, File> scanFilesByResource = fileZipUtils.buildZipByCategory(_projectId,scanFiles, baseDir);
	    				 Enumeration<String> scanResourceEnumerator = scanFilesByResource.keys();
	    				 while (scanResourceEnumerator.hasMoreElements()) {
	    					 String resource = scanResourceEnumerator.nextElement();
	    					 String format = resource;
	    					 String content = resource;
	    					 String tag = resource;
	    					 
	    					 if (resource.equals(BIDSConstants.NIFTI_RESOURCE)) {
	    						 format =  BIDSConstants.NIFTI_RESOURCE + "_RAW";
	    						 content =  BIDSConstants.NIFTI_RESOURCE + "_RAW";
	    					 }
	    					 File zipFile = scanFilesByResource.get(resource);
	    					 if (zipFile != null && zipFile.exists()) {
	    						 String url_postfix =  "/data/archive/projects/" + _projectId + "/subjects/" + mr.getSubjectId() + "/experiments/" +mr.getLabel() + "/scans/" + scan.getId() + "/resources/" + resource + "/files?overwrite=true&extract=true&format="+format+"&content="+content+"&tags=BIDS";
	    	   					 response = _manager.importResource(connection, url_postfix , zipFile);
				                	if (!response.wasSuccessful()) {
				                		ImportManager.UPDATE_MANIFEST(_projectId, "Scan files import failed for MR: " + mr.getLabel() + " Scan: " + scan.getId()  + " Resource: " + resource);
				                	}else {
				                		ImportManager.UPDATE_MANIFEST(_projectId, "Scan files import successed for MR: " + mr.getLabel() + " Scan: " + scan.getId()  + " Resource: " + resource);
				                	}
	    	   					    						 
	    					 }
	    				 }
	    			}
	     		 }
	     		//Get all the Behavioral files associated
	     		File[] _subDirectories = baseDir.listFiles(new FilenameFilter() {
	     		    public boolean accept(File dir, String name) {
	     		    	File subDir = new File(dir, name);
	     		    	boolean acceptMe = false;
	     		    	if (subDir.isDirectory()) {
	     		    		acceptMe = true;
	     		    	}
	     		    	return acceptMe;
	     		    }
	     		});
	     		if (_subDirectories!= null && _subDirectories.length > 0) {
	     			for (File sDir : _subDirectories) {
	     				String dataType = sDir.getName();
	     				File[] niftiFilesWithinDataType = sDir.listFiles(new NiftiFileFilter()); 
	     				//Each nifti file creates a scan of type dataType
	     				if (niftiFilesWithinDataType == null || niftiFilesWithinDataType.length == 0) {
	     					File[] _subDirFiles = sDir.listFiles(new FilenameFilter() {
	     					    public boolean accept(File dir, String name) {
	     					    	File subDir = new File(dir, name);
	     					    	boolean acceptMe = false;
	     					    	if (subDir.isFile()) {
	     					    		acceptMe = true;
	     					    	}
	     					    	return acceptMe;
	     					    }
	     					});
	     					if (_subDirFiles != null && _subDirFiles.length > 0) {
	     						FileZipUtils fileZipUtils = new FileZipUtils();
	     						String expCachePath = ImportManager.GET_SYNC_FILE_PATH(_projectId);
	    						File zipFile = new File(expCachePath, dataType + "_" +(new Date()).getTime()+".zip");
	    						List<File> filesAsList = new ArrayList<File>();
	    						filesAsList.addAll(Arrays.asList(_subDirFiles));
	    						fileZipUtils.createZipFile(zipFile,filesAsList, baseDir);
		    					 if (zipFile != null && zipFile.exists()) {
		    						 String url_postfix =  "/data/archive/projects/" + _projectId + "/subjects/" + mr.getSubjectId() + "/experiments/" +mr.getLabel() + "/resources/" + dataType + "/files?overwrite=true&extract=true&content="+dataType+"&tags=BIDS";
		    	   					 response = _manager.importResource(connection, url_postfix , zipFile);
					                	if (!response.wasSuccessful()) {
					                		ImportManager.UPDATE_MANIFEST(_projectId, "Resource files import failed for MR: " + mr.getLabel() +  " Resource: " + dataType);
					                	}else {
					                		ImportManager.UPDATE_MANIFEST(_projectId, "Resource files import successed for MR: " + mr.getLabel() + " Resource: " + dataType);
					                		zipFile.deleteOnExit();
					                	}
		    	   					    						 
		    					 }

	     					}
	     					
	     				}
	     			}
	     		}

	     		 
			 }
			 
		}catch(Exception e) {
			System.out.println(e.getMessage());
			 ImportManager.SAVE_FAILED_UPLOAD(mr);
			 throw e;
		}
	}
	
	private String extractSubStrAfter(String subjectFolderName, String subStrAfter) {
		String subjectLabel = null;
		if (subjectFolderName != null) {
			if (subjectFolderName.startsWith(subStrAfter)) {
				subjectLabel = subjectFolderName.substring(subStrAfter.length());
			}
		}
		return subjectLabel;
	}


	private void participantTSVToProperties() {
		participantProperties = new ArrayList<Properties>();
		if (_participantTSV != null && _participantTSV.exists() && _participantTSV.isFile()) {
			String[] columnHeaders = null;
			try {
				BufferedReader bReader = new BufferedReader(new FileReader(_participantTSV));
		        String line;
		        /**
		         * Looping the read block until all lines in the file are read.
		         */
		        int firstLine = 0;
		        while ((line = bReader.readLine()) != null) {
		            /**
		             * Splitting the content of tabbed separated line
		             */
		            String datavalue[] = line.split("\\t");
		            firstLine++;
		            if (firstLine == 1) {
		            	if (datavalue != null && datavalue.length > 0) {
		            		columnHeaders=datavalue;
			            	boolean validColumns = false;
			            	for(String col:columnHeaders) {
			            		if (col.equals(BIDSConstants.PARTICIPANT_ID)) {
			            			validColumns = true;
			            			break;
			            		}
			            	}
			            	if (!validColumns) {
			            		break;
			            	}
			            	for (String col:columnHeaders) {
			            		if (col.toUpperCase().equals("AGE")) {
			            			participantTSVHasAge = true;
			            			break;
			            		}
			            	}
		            	}
		            }else {
		            	if (datavalue != null && datavalue.length > 0) {
		            		Properties prop = new Properties();
		            		if (columnHeaders != null) {
			            		for (int i=0; i<datavalue.length; i++) {
			                		prop.setProperty(columnHeaders[i], datavalue[i]);
			                	}
			                	participantProperties.add(prop);
		            		}
		            	}
		            }
		        }
		        bReader.close();
			}catch(FileNotFoundException fne) {
				System.out.println("File Not Found " + _participantTSV.getAbsolutePath());
				System.exit(1);
			}catch(IOException ioe) {
				System.out.println("IO Exception while reading file  " + _participantTSV.getAbsolutePath());
				System.exit(1);
			}catch(Exception e) {
				
			}
		}
	}
	
	private String getSubjectAgeAtExperiment(String subjectLabel) {
		String participant_id = BIDSConstants.SUBJECT_PREFIX + subjectLabel;
		String age = null;
		for (Properties p:participantProperties) {
			String p_id = p.getProperty(BIDSConstants.PARTICIPANT_ID);
			if (participant_id.equals(p_id)) {
				if (p.containsKey("age")) {
					age = p.getProperty("age");
				}else if (p.containsKey("AGE")) {
					age = p.getProperty("AGE");
				}else if (p.containsKey("Age")) {
					age = p.getProperty("Age");
				}
				break;
			}
		}
		return age;
	}

}
