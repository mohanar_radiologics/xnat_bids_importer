package com.radiologics.bids.dataimport;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

import org.nrg.xdat.bean.XnatMrscandataBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ImportScans {
	
	File _mrRootFolder;
	String _subjectLabel;
	String _mrLabel;
	File _derivatesRootFolder;
	File[] _subDirectories;
	File[] _subDirFiles;
	List<File> _projectFolderLevelJsons;
    private static final Logger _log = LoggerFactory.getLogger(ImportScans.class);

	
	public ImportScans(File mrRootFolder, String subjectLabel, String mrLabel, File derivatesRootFolder, List<File> projectFolderLevelJsons) {
		_mrRootFolder = mrRootFolder;
		_subjectLabel = subjectLabel;
		_mrLabel = mrLabel;
		_derivatesRootFolder = derivatesRootFolder;
		_projectFolderLevelJsons = projectFolderLevelJsons;
	}
	
	public Hashtable<XnatMrscandataBean, Hashtable<String,List<File>>> getScans() {
		Hashtable<XnatMrscandataBean, Hashtable<String,List<File>>> scanFileHash = new Hashtable<XnatMrscandataBean, Hashtable<String,List<File>>>();
		//Traverse all folders within the subjectRootFolder looking for scan files. 
		_subDirectories = _mrRootFolder.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		    	File subDir = new File(dir, name);
		    	boolean acceptMe = false;
		    	if (subDir.isDirectory()) {
		    		acceptMe = true;
		    	}
		    	return acceptMe;
		    }
		});
		_subDirFiles = _mrRootFolder.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		    	File subDir = new File(dir, name);
		    	boolean acceptMe = false;
		    	if (subDir.isFile()) {
		    		acceptMe = true;
		    	}
		    	return acceptMe;
		    }
		});
		
		// As per Lauren Wallace
		//The scan number is the BIDS folder (anat, func, dwi, etc) plus a simple counter from first to last file in the folder,
		//the scan type is the BIDS type element (last thing before the file extension), 
		//and the series description is a more complete version of the BIDS file name including run number and additional info

		if (_subDirectories!= null && _subDirectories.length > 0) {
			Hashtable<String, Integer> counter = new Hashtable<String, Integer>();
			for (File sDir : _subDirectories) {
				String dataType = sDir.getName();
				File[] niftiFilesWithinDataType = sDir.listFiles(new NiftiFileFilter()); 
				//Each nifti file creates a scan of type dataType
				if (niftiFilesWithinDataType != null && niftiFilesWithinDataType.length > 0) {
					for (File niftiFile:niftiFilesWithinDataType) {
					  List<File> niftiscanFiles = new ArrayList<File>();
					  niftiscanFiles.add(niftiFile);
					  List<File> bidsscanFiles = new ArrayList<File>();
					  List<File> derivativesScanFiles = new ArrayList<File>();
					  String niftiFileName = niftiFile.getName();
					  Hashtable<String,String> parsedTokens = parseTokensInFileBaseName(niftiFileName);

					  //scanFiles.add(niftiFile);
					  Integer cnt = 	getNextCount(dataType,counter,parsedTokens);
					  setNextCount(dataType,counter,cnt);
					  String scanId = dataType + cnt;
					  
					  String scanType = parsedTokens.get(BIDSConstants.MODALITY_LABEL);

					  String scanSeriesDescription = this.getSeriesDescription(parsedTokens);
					  XnatMrscandataBean mrScan = new XnatMrscandataBean();
					  
					  mrScan.setId(scanId);
					  mrScan.setType(scanType);
					  mrScan.setSeriesDescription(scanSeriesDescription);
					  mrScan.setModality(scanType);
					  if (parsedTokens.contains(BIDSConstants.ACQ_PREFIX)) {
						  mrScan.setParameters_acqtype(parsedTokens.get(BIDSConstants.ACQ_PREFIX));
					  }
					  
					  String niftiBaseFileName = null;
					   int indexOfFirstDot = niftiFileName.indexOf(".");
					   if (indexOfFirstDot != -1) {
						   niftiBaseFileName = niftiFileName.substring(0,indexOfFirstDot);
					   }
					   List<File> allRelatedFiles = getAccompanyingFiles(sDir,niftiBaseFileName, parsedTokens);
					   //niftiscanFiles.addAll(allRelatedFiles);
					   bidsscanFiles.addAll(allRelatedFiles);
					   
					   //Get any json file names with same niftiBaseFileName
					   File jsonFile = getAccompanyingJsonFile(sDir,niftiBaseFileName + BIDSConstants.JSON_EXT);
					   if (jsonFile != null) {
						   //scan parameters are in this file
						   ScanParamsFromJSON scanParamFromJson = new ScanParamsFromJSON(mrScan,jsonFile);
						   scanParamFromJson.populate();
						   bidsscanFiles.add(jsonFile);
					   }else {
						   //Does a json file applicable to all scans exist?
						   boolean fileExists = false;
						   List<String> possibleFileNames = getCommonJSONFileName(parsedTokens);
						   for (String possibleFileName : possibleFileNames ) {
							   File commonFile = new File(sDir, possibleFileName);
							   if (commonFile.exists() && commonFile.isFile()) {
								   fileExists = true;
								   ScanParamsFromJSON scanParamFromJson = new ScanParamsFromJSON(mrScan,jsonFile);
								   scanParamFromJson.populate();
								   bidsscanFiles.add(commonFile);
							   }
							   break;
						   }
						   if (!fileExists){
							   //Does a Global JSON file exist?
							   jsonFile = getGlobalJSONFile(parsedTokens);
							   if (jsonFile != null) {
								   ScanParamsFromJSON scanParamFromJson = new ScanParamsFromJSON(mrScan,jsonFile);
								   scanParamFromJson.populate();
								   //bidsscanFiles.add(jsonFile);
							   }
						   }
					   }
					   if (_derivatesRootFolder != null ) {
						   //Now look to see if there are any derivative files
							File scanDerivativeSubFolder = new File(_derivatesRootFolder.getAbsolutePath() + File.separator + BIDSConstants.SUBJECT_PREFIX + this._subjectLabel + File.separator  + scanType);
							if (scanDerivativeSubFolder != null && !scanDerivativeSubFolder.exists()) {
								   String[] mrNameParts = _mrLabel.split("_");
								   if (mrNameParts != null && mrNameParts.length > 1) {
									   String mrLabelOriginal = mrNameParts[1].substring(2);
										scanDerivativeSubFolder = new File(_derivatesRootFolder.getAbsolutePath() + File.separator + BIDSConstants.SUBJECT_PREFIX + this._subjectLabel + File.separator + BIDSConstants.SESSION_PREFIX + mrLabelOriginal + File.separator + scanType);
								   }
							}
							if (scanDerivativeSubFolder != null && scanDerivativeSubFolder.exists() && scanDerivativeSubFolder.isDirectory()) {
							//All files in this directory are resources files of the scan
							   //allRelatedFiles = getAccompanyingFiles(scanDerivativeSubFolder,niftiBaseFileName, parsedTokens);
							   File[] allDerivativeFiles = scanDerivativeSubFolder.listFiles();
							   if (allDerivativeFiles != null && allDerivativeFiles.length > 0) {
								derivativesScanFiles.addAll(Arrays.asList(allDerivativeFiles));
							   }
							}
					   }
						Hashtable<String, List<File>> scanFilesByCategory = new Hashtable<String, List<File>>();
						if (niftiscanFiles.size() > 0) scanFilesByCategory.put(BIDSConstants.NIFTI_RESOURCE, niftiscanFiles);
						if (bidsscanFiles.size() > 0) scanFilesByCategory.put(BIDSConstants.BIDS_RESOURCE, bidsscanFiles);
						if (derivativesScanFiles.size() > 0) scanFilesByCategory.put(BIDSConstants.DERIVATIVES_RESOURCE, derivativesScanFiles);
						scanFileHash.put(mrScan, scanFilesByCategory);
					}
					
				}
			}
		}
		return scanFileHash;
	}
	
	private File getGlobalJSONFile(Hashtable<String,String> parsedTokens) {
		File jsonFile = null;
		String modality = parsedTokens.get(BIDSConstants.MODALITY_LABEL);
		boolean found = false;
		for (File f: _projectFolderLevelJsons) {
			if (f.getName().equals(modality+BIDSConstants.JSON_EXT)) {
				jsonFile = f;
				found = true;
				break;
			}
		}
		if (!found) {
			String fileName = BIDSConstants.TASK_PREFIX + parsedTokens.get(BIDSConstants.TASK_PREFIX) +"_" + modality;
			for (File f: _projectFolderLevelJsons) {
				if (f.getName().equals(fileName+BIDSConstants.JSON_EXT)) {
					jsonFile = f;
					found = true;
					break;
				}
			}
		}
		return jsonFile;
	}
	
	private String getGlobalJSONFileNameByModality(Hashtable<String,String> parsedTokens) {
		String subject = parsedTokens.get(BIDSConstants.SUBJECT_PREFIX);
		String fileName = subject;

		String modality = parsedTokens.get(BIDSConstants.MODALITY_LABEL);
		String task = parsedTokens.get(BIDSConstants.TASK_PREFIX);
		if (task != null) {
			fileName += "_task-" + task;
			
		}else if (modality.equals("dwi")) {
			
		}else if (modality.equals("dwi")) {
			
		}
		return fileName+".json";
	}
	
	private List<String> getCommonJSONFileName(Hashtable<String,String> parsedTokens) {
		List<String> possibleJsonFiles = new ArrayList<String>();
		String modality = parsedTokens.get(BIDSConstants.MODALITY_LABEL);
		String fileName = "";
		if (parsedTokens.containsKey(BIDSConstants.TASK_PREFIX)) {
			fileName += BIDSConstants.TASK_PREFIX +parsedTokens.get(BIDSConstants.TASK_PREFIX) +"_";
			possibleJsonFiles.add(fileName+modality+BIDSConstants.JSON_EXT);
			if (parsedTokens.containsKey(BIDSConstants.ACQ_PREFIX)) {
				fileName += BIDSConstants.ACQ_PREFIX +parsedTokens.get(BIDSConstants.ACQ_PREFIX) +"_";
			}
			if (parsedTokens.containsKey(BIDSConstants.RUN_PREFIX)) {
				fileName += BIDSConstants.RUN_PREFIX +parsedTokens.get(BIDSConstants.RUN_PREFIX) +"_";
			}
			possibleJsonFiles.add(fileName+modality+BIDSConstants.JSON_EXT);
		}else {
			fileName += modality + BIDSConstants.JSON_EXT;
		}
		return possibleJsonFiles;
	}
	
	private String getSeriesDescription(Hashtable<String,String> parsedTokens) {
		String seriesDescription ="";
		if (parsedTokens.containsKey(BIDSConstants.TASK_PREFIX)) {
			seriesDescription += BIDSConstants.TASK_PREFIX +parsedTokens.get(BIDSConstants.TASK_PREFIX) +"_";
		}
		if (parsedTokens.containsKey(BIDSConstants.ACQ_PREFIX)) {
			seriesDescription += BIDSConstants.ACQ_PREFIX +parsedTokens.get(BIDSConstants.ACQ_PREFIX) +"_";
		}
		if (parsedTokens.containsKey(BIDSConstants.REC_PREFIX)) {
			seriesDescription += BIDSConstants.REC_PREFIX +parsedTokens.get(BIDSConstants.REC_PREFIX) +"_";
		}
		if (parsedTokens.containsKey(BIDSConstants.RUN_PREFIX)) {
			seriesDescription += BIDSConstants.RUN_PREFIX +parsedTokens.get(BIDSConstants.RUN_PREFIX) +"_";
		}
		if (parsedTokens.containsKey(BIDSConstants.ECHO_PREFIX)) {
			seriesDescription += BIDSConstants.ECHO_PREFIX +parsedTokens.get(BIDSConstants.ECHO_PREFIX) +"_";
		}
		if (parsedTokens.containsKey(BIDSConstants.MOD_PREFIX)) {
			seriesDescription += BIDSConstants.MOD_PREFIX +parsedTokens.get(BIDSConstants.MOD_PREFIX) +"_";
		}
		if (seriesDescription.equals("")) {
			if (parsedTokens.containsKey(BIDSConstants.MODALITY_LABEL)) {
				seriesDescription += parsedTokens.get(BIDSConstants.MODALITY_LABEL);
			}
		}
		if (seriesDescription.endsWith("_")) {
			seriesDescription = seriesDescription.substring(0, seriesDescription.length()-1);
		}

			return seriesDescription;
	}
	
	private Hashtable<String,String> parseTokensInFileBaseName(String fileName) {
		//sub-<participant_label>[_ses-<session_label>]_task-<task_label>[_acq-<label>][_rec-<label>][_run-<index>][_echo-<index>]_bold.nii[.gz]
		String[] tokens = fileName.split("\\.");
		String baseFileName = tokens[0];
		String[] fileNameTokens = baseFileName.split("_");
		Hashtable<String,String> parsedTokens = new Hashtable<String, String>();
		for (int i=0; i< fileNameTokens.length;i++) {
			if (fileNameTokens[i].startsWith(BIDSConstants.SUBJECT_PREFIX)) {
				parsedTokens.put(BIDSConstants.SUBJECT_PREFIX, fileNameTokens[i].substring(BIDSConstants.SUBJECT_PREFIX.length()));
			}else if (fileNameTokens[i].startsWith(BIDSConstants.SESSION_PREFIX)) {
				parsedTokens.put(BIDSConstants.SESSION_PREFIX, fileNameTokens[i].substring(BIDSConstants.SESSION_PREFIX.length()));
			}else if (fileNameTokens[i].startsWith(BIDSConstants.TASK_PREFIX)) {
				parsedTokens.put(BIDSConstants.TASK_PREFIX, fileNameTokens[i].substring(BIDSConstants.TASK_PREFIX.length()));
			}else if (fileNameTokens[i].startsWith(BIDSConstants.ACQ_PREFIX)) {
				parsedTokens.put(BIDSConstants.ACQ_PREFIX, fileNameTokens[i].substring(BIDSConstants.ACQ_PREFIX.length()));
			}else if (fileNameTokens[i].startsWith(BIDSConstants.REC_PREFIX)) {
				parsedTokens.put(BIDSConstants.REC_PREFIX, fileNameTokens[i].substring(BIDSConstants.REC_PREFIX.length()));
			}else if (fileNameTokens[i].startsWith(BIDSConstants.RUN_PREFIX)) {
				parsedTokens.put(BIDSConstants.RUN_PREFIX, fileNameTokens[i].substring(BIDSConstants.RUN_PREFIX.length()));
			}else if (fileNameTokens[i].startsWith(BIDSConstants.ECHO_PREFIX)) {
				parsedTokens.put(BIDSConstants.ECHO_PREFIX, fileNameTokens[i].substring(BIDSConstants.ECHO_PREFIX.length()));
			}else if (fileNameTokens[i].startsWith(BIDSConstants.MOD_PREFIX)) {
				parsedTokens.put(BIDSConstants.MOD_PREFIX, fileNameTokens[i].substring(BIDSConstants.MOD_PREFIX.length()));
			}else {
				parsedTokens.put(BIDSConstants.MODALITY_LABEL, fileNameTokens[i]);
			}

		}
		return parsedTokens;
	}
	
	private List<File> getAccompanyingFiles(File dir, String niftiFileBaseName,Hashtable<String,String> parsedTokens) {
		List<File> accompanyingFiles = new ArrayList<File>();
		HashSet<File> fileSet = new HashSet<File>();
		File[] f = dir.listFiles(new NiftiBaseFileNameFilter(niftiFileBaseName,true));
		fileSet.addAll(Arrays.asList(f));
		//Take care of other ways of naming a file
		//Strip modality from filename
		String rootTaskFileName =  niftiFileBaseName;
		if(parsedTokens.get(BIDSConstants.MODALITY_LABEL) != null) {
			int modalityIndex = niftiFileBaseName.lastIndexOf(parsedTokens.get(BIDSConstants.MODALITY_LABEL));
			if (modalityIndex != -1) {
				rootTaskFileName = niftiFileBaseName.substring(0,modalityIndex);
				if (rootTaskFileName.endsWith("_")) 
					rootTaskFileName = rootTaskFileName.substring(0, rootTaskFileName.length() -1);
			}
		}
		f = dir.listFiles(new ExactRootFileNameFilter(rootTaskFileName+ "_events"));
		if (f != null && f.length > 0)
			fileSet.addAll(Arrays.asList(f));
		f = dir.listFiles(new ExactRootFileNameFilter(rootTaskFileName+ "_physio"));
		if (f != null && f.length > 0)
			fileSet.addAll(Arrays.asList(f));
		f = dir.listFiles(new ExactRootFileNameFilter(rootTaskFileName+ "_stim"));
		if (f != null && f.length > 0)
			fileSet.addAll(Arrays.asList(f));

		String fileName =BIDSConstants.SUBJECT_PREFIX + parsedTokens.get(BIDSConstants.SUBJECT_PREFIX) +"_";
		if (parsedTokens.containsKey(BIDSConstants.TASK_PREFIX)) {
			String taskFileName = fileName + BIDSConstants.TASK_PREFIX +parsedTokens.get(BIDSConstants.TASK_PREFIX) +"_";
			fileName += BIDSConstants.TASK_PREFIX +parsedTokens.get(BIDSConstants.TASK_PREFIX) +"_";
			if (parsedTokens.containsKey(BIDSConstants.ACQ_PREFIX)) {
				fileName += BIDSConstants.ACQ_PREFIX +parsedTokens.get(BIDSConstants.ACQ_PREFIX) +"_";
			}
			if (parsedTokens.containsKey(BIDSConstants.RUN_PREFIX)) {
				fileName += BIDSConstants.RUN_PREFIX +parsedTokens.get(BIDSConstants.RUN_PREFIX) +"_";
			}
			if (fileName.endsWith("_")) {
				fileName = fileName.substring(0, fileName.length() -1 );
			}
			if (taskFileName.endsWith("_")) {
				taskFileName = taskFileName.substring(0, taskFileName.length() -1 );
			}

			f = dir.listFiles(new NiftiBaseFileNameFilter(fileName,true));
			if (f !=null && f.length > 0) {
				fileSet.addAll(Arrays.asList(f));
			}else {
				f = dir.listFiles(new NiftiBaseFileNameFilter(taskFileName,true));
				if (f !=null && f.length > 0) {
					fileSet.addAll(Arrays.asList(f));
				}
				
			}
		}
		accompanyingFiles.addAll(fileSet);
		return accompanyingFiles;
	}
	
	private File getAccompanyingJsonFile(File dir, String jsonFileName) {
		File jsonFile = null;
		File[] accompanyingFiles = dir.listFiles(new ExactFileNameFilter(jsonFileName));
		if (accompanyingFiles != null && accompanyingFiles.length > 0) {
			jsonFile = accompanyingFiles[0];
		}
		return jsonFile;
	}
	
	private Integer getNextCount(String dataType, Hashtable<String, Integer> counter,Hashtable<String,String> parsedTokens) {
		Integer nextCount = 1;
		Integer existingCount = counter.get(dataType);
		if (existingCount != null) { 
			nextCount = ++existingCount;
		}

/*		if (parsedTokens.containsKey(BIDSConstants.RUN_PREFIX)) {
			try {
				nextCount = Integer.parseInt(parsedTokens.get(BIDSConstants.RUN_PREFIX)) ;
			}catch(NumberFormatException nfe) {
				
			}
		}else {
			Integer existingCount = counter.get(dataType);
			if (existingCount != null) { 
				nextCount = ++existingCount;
			}
		}
*/		return nextCount;
	}

	private void setNextCount(String dataType, Hashtable<String, Integer> counter, Integer cnt) {
		counter.put(dataType, cnt);
	}
	
	   // FileNameFilter implementation
		public static class NiftiBaseFileNameFilter implements FilenameFilter {

			private String _basename;
			private boolean _skipNifti;

			
			NiftiBaseFileNameFilter(String basename, boolean skip) {
				_basename = basename;
				_skipNifti = skip;
			}




			@Override
			public boolean accept(File dir, String name) {
//				return name.startsWith(_basename);
		    	File s = new File(dir, name);
		    	boolean acceptMe = false;
		    	if (s.isFile()) {
		    		if (_skipNifti ) {
		    			if (name.startsWith(_basename) && !name.endsWith(BIDSConstants.NII_EXT) && !name.endsWith(BIDSConstants.NII_GZ_EXT) && !name.endsWith(".json")) {
				    		acceptMe = true;
		    			}
		    		}else {
		    			if (name.startsWith(_basename) && name.endsWith(BIDSConstants.NII_EXT) || name.endsWith(BIDSConstants.NII_GZ_EXT) && !name.endsWith(".json")) {
				    		acceptMe = true;
		    			}
		    		}
		    	}
		    	return acceptMe;

			}

		}

		   // FileNameFilter implementation
			public static class NiftiFileFilter implements FilenameFilter {
				public NiftiFileFilter() {}

				@Override
				public boolean accept(File dir, String name) {
				    	File s = new File(dir, name);
				    	boolean acceptMe = false;
				    	if (s.isFile() && (name.endsWith(BIDSConstants.NII_EXT) || name.endsWith(BIDSConstants.NII_GZ_EXT))) {
				    		acceptMe = true;
				    	}
				    	return acceptMe;
				    }
			}

	
		   // FileNameFilter implementation
			public static class ExactFileNameFilter implements FilenameFilter {

				private String _name;

				public ExactFileNameFilter(String name) {
					_name = name;
				}

				@Override
				public boolean accept(File dir, String name) {
					return name.equals(_name);
				}

			}

			   // FileNameFilter implementation
				public static class ExactRootFileNameFilter implements FilenameFilter {

					private String _name;

					public ExactRootFileNameFilter(String name) {
						_name = name;
					}

					@Override
					public boolean accept(File dir, String name) {
						boolean acceptMe = false;
						String rootFileName = name;
						int lastIndexOfDot = name.lastIndexOf(".");
						if (lastIndexOfDot != -1) {
							rootFileName = name.substring(0,lastIndexOfDot);
							acceptMe = rootFileName.equals(_name);
						}else {
							acceptMe = name.equals(_name);
						}
						return acceptMe;
					}

				}

}
