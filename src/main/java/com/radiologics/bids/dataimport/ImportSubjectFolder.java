package com.radiologics.bids.dataimport;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ImportSubjectFolder {
	
	File _subjectFolder;
	String _participantLabel;
	File _sessionTSVFile;
	
	public ImportSubjectFolder(File subjectFolder) {
		_subjectFolder = subjectFolder;
		setParticipantLabel();
	}
	
	private void setParticipantLabel() {
		String subjectFolderName = _subjectFolder.getName();
		if (subjectFolderName.startsWith(BIDSConstants.SUBJECT_PREFIX)) {
			_participantLabel = subjectFolderName.substring(BIDSConstants.SUBJECT_PREFIX.length());
		}
		
		//Look for subject sessions TSV file for longitudinal studies
		File[] sessionTSVFiles = _subjectFolder.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		    	File subDir = new File(dir, name);
		    	boolean acceptMe = false;
		    	if (subDir.isFile()) {
		    		acceptMe = name.equals(BIDSConstants.SUBJECT_PREFIX  + _participantLabel + "_sessions.tsv" ); 
		    	}
		    	return acceptMe;
		    }
		});
		
		if (sessionTSVFiles != null && sessionTSVFiles.length == 1) {
			_sessionTSVFile = sessionTSVFiles[0];
		}
		List<File> experimentFolders = new ArrayList<File>();
		
		if (_sessionTSVFile == null) {
			//No session information is provided. Look for session folders
			//Descend into the folder to see if a session label exists
			File[] sessionSubFolders = _subjectFolder.listFiles(new FilenameFilter() {
			    public boolean accept(File dir, String name) {
			    	File subDir = new File(dir, name);
			    	boolean acceptMe = false;
			    	if (subDir.isDirectory()) {
			    		acceptMe = name.startsWith(BIDSConstants.SESSION_PREFIX); 
			    	}
			    	return acceptMe;
			    }
			});
			if (sessionSubFolders != null && sessionSubFolders.length > 0) {
				experimentFolders.addAll(Arrays.asList(sessionSubFolders));
			}else {
				//All session files are dumped into one folder. Create session
				experimentFolders.add(_subjectFolder);
			}
		}else {
			//Session Labels and information about the session is provided
			//Parse the TSV file
			
		}
		
	}
	
	
}
