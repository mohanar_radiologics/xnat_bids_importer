package com.radiologics.bids.dataimport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.nrg.xdat.bean.XnatMrsessiondataBean;

import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.dataimport.manager.ImportManager;
import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ImportSubjectSessions {
    private final RemoteConnectionManager    _manager;
	File _sessionTSVFile;
	File _dataRootFolder;
	String[] columnHeaders ;
	RemoteConnection _connection;
	
	
	public ImportSubjectSessions(RemoteConnectionManager manager, RemoteConnection connection, File sessionTSVFile, File rootFolder) {
		_manager = manager;
		_connection = connection;
		_sessionTSVFile = sessionTSVFile;
		_dataRootFolder = rootFolder;
	}

	public void uploadSession(ImportObserver observer, String projectId, String subjectLabel) {
		if (_sessionTSVFile.exists() && _sessionTSVFile.isFile()) {
			try {
				BufferedReader bReader = new BufferedReader(new FileReader(_sessionTSVFile));
		        String line;
		        /**
		         * Looping the read block until all lines in the file are read.
		         */
		        int firstLine = 0;
		        while ((line = bReader.readLine()) != null) {
		            /**
		             * Splitting the content of tabbed separated line
		             */
		            String datavalue[] = line.split("\\t");
		            firstLine++;
		            if (firstLine == 1) {
		            	if (datavalue != null && datavalue.length > 0) {
		            		columnHeaders=datavalue;
		            	}
		            	boolean validColumns = false;
		            	for(String col:columnHeaders) {
		            		if (col.equals(BIDSConstants.SESSION_ID)) {
		            			validColumns = true;
		            			break;
		            		}
		            	}
		            	if (!validColumns) {
		            		break;
		            	}
		            }else {
		            	if (datavalue != null && datavalue.length > 0) {
		                	Properties prop = new Properties();
		                	for (int i=0; i<datavalue.length; i++) {
		                		prop.setProperty(columnHeaders[i], datavalue[i]);
		                	}
		                	SessionRow sessionRow = new SessionRow(prop);
		                	XnatMrsessiondataBean session = sessionRow.toXnatMrSessionDataBean();
		                	session.setProject(projectId);
		                	String subjectAccessionId = ImportManager.GET_SUBJECT_XNAT_ID(projectId, subjectLabel);
		                	if (subjectAccessionId != null) {
		                		session.setSubjectId(subjectAccessionId);
		                	}
		                	//Now add all the scans for this session
		                	//Then upload the data
		                	
		            	}
		            }
		        }
		        bReader.close();
			}catch(FileNotFoundException fne) {
				System.out.println("File Not Found " + _sessionTSVFile.getAbsolutePath());
				System.exit(1);
			}catch(IOException ioe) {
				System.out.println("IO Exception while reading file  " + _sessionTSVFile.getAbsolutePath());
				System.exit(1);
			}catch(Exception e) {
				System.out.println("Exception while reading file  " + _sessionTSVFile.getAbsolutePath() + " " + e.getMessage());
				System.exit(1);
			}
		}

	}
}
