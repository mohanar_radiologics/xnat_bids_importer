package com.radiologics.bids.dataimport;

import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Properties;

import org.nrg.xdat.bean.XnatDemographicdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataFieldBean;

import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ParticipantRow {
	
	Properties _properties = new Properties();

	public ParticipantRow(Properties properties) {
		_properties = properties;
	}
	
	public XnatSubjectdataBean toSubjectBean() {
		XnatSubjectdataBean subject = new XnatSubjectdataBean();
		String participant_id = _properties.getProperty(BIDSConstants.PARTICIPANT_ID);
		String subjectLabel = clearPrefix(participant_id);
		subject.setLabel(subjectLabel);
		try {
			Enumeration<?> e = _properties.keys();
			XnatDemographicdataBean demoData = new XnatDemographicdataBean();
			boolean hasDemo = false;
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				String value = _properties.getProperty(key);
				if (BIDSConstants.SubjectDemographicsMap.containsKey(key) || BIDSConstants.SubjectDemographicsMap.containsKey(key.toUpperCase())) {
					hasDemo = true;
					String methodName = "set" + key.toLowerCase().substring(0, 1).toUpperCase() + key.toLowerCase().substring(1);
				    Method setNameMethod = demoData.getClass().getMethod(methodName, String.class);
				    try {
				    	String v  = value;
				    	if (key.toUpperCase().equals("GENDER")) {
				    		v = getGenderForXnat(value);
				    	}
				    	setNameMethod.invoke(demoData, v); // pass arg
				    }catch(Exception ex) {
				    	ex.printStackTrace();
				    }
				}else {
					if (key.toUpperCase().equals("SEX")) {
						demoData.setGender(getGenderForXnat(value));
					}else {
						XnatSubjectdataFieldBean subjectField = new XnatSubjectdataFieldBean();
						subjectField.setName(key);
						subjectField.setField(value);
						subject.addFields_field(subjectField);
					}
				}
			}
			if (hasDemo) {
				subject.setDemographics(demoData);
			}
		}catch(Exception e) {
			
		}
		return subject;
	}
	
	private String clearPrefix(String label) {
		String cleanedLabel = label;
		if (label.startsWith(BIDSConstants.SUBJECT_PREFIX)) {
			cleanedLabel = label.substring(4);
		}
		return cleanedLabel;
	}
	
	private String getGenderForXnat(String gender) {
		String g=gender;
		if (gender == null) return g;
		if (gender.toUpperCase().equals("FEMALE") ) {
			g="female";
		}else if (gender.toUpperCase().equals("MALE")) {
			g="male";
		}else if (gender.toUpperCase().equals("OTHER")) {
			g="other";
		}else if (gender.toUpperCase().equals("UNKNOWN")) {
			g="unknown";
		}else if (gender.toUpperCase().equals("F") ) {
			g="F";
		}else if (gender.toUpperCase().equals("M")) {
			g="M";
		}
		return g;
	}
}
