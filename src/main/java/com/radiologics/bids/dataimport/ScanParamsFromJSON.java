package com.radiologics.bids.dataimport;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;

import org.nrg.xdat.bean.XnatAddfieldBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ScanParamsFromJSON {
	
	File _jsonFile;
	XnatMrscandataBean _mrScan;
    private static final Logger _log = LoggerFactory.getLogger(ScanParamsFromJSON.class);


	public ScanParamsFromJSON(XnatMrscandataBean mrScan, File jsonFile) {
		_mrScan = mrScan;
		_jsonFile = jsonFile;
	}
	
	public void populate() {
		if (_jsonFile != null) {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				JsonNode rootNode = objectMapper.readTree(_jsonFile);
				Iterator<String> fieldNamesIterator = rootNode.fieldNames();
				while (fieldNamesIterator.hasNext()) {
					String fieldName = fieldNamesIterator.next();
					JsonNode field = rootNode.get(fieldName);
					if (field != null) {
						if (BIDSConstants.BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.containsKey(fieldName)) {
							String xnatParamName = BIDSConstants.BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.get(fieldName);
							String methodName = "set" + xnatParamName;
						    Method setNameMethod = _mrScan.getClass().getMethod(methodName, String.class);
						    try {
						    	setNameMethod.invoke(_mrScan, field.asText()); // pass arg
						    }catch(Exception e) {
						    	_log.warn("Could not set the XNAT Scan Param " + methodName + " value: " + field.asText() + " for scan " + _mrScan.getId() );
						    }
						    addParam(field, fieldName);
						}else {
						    addParam(field, fieldName);
						}
					}
				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	private void addParam(JsonNode field, String fieldName) {
		String fieldText = jsonNodeTextToString(field);
		XnatAddfieldBean addField = new XnatAddfieldBean();
		addField.setName(fieldName);
		addField.setAddfield(fieldText);
		_mrScan.addParameters_addparam(addField);
	}
	
	private String jsonNodeTextToString(JsonNode node) {
		String nodeToStr = "";
		if (node.isArray()) {
			nodeToStr = "[";
			Iterator<JsonNode> it = node.iterator();
			while (it.hasNext()) {
				JsonNode n = it.next();
				nodeToStr += n.asText() + ",";
			}
			if (nodeToStr.endsWith(",")) {
				nodeToStr = nodeToStr.substring(0,nodeToStr.length() -1);
			}
			nodeToStr += "]";
		}else {
			nodeToStr = node.asText();
			if (nodeToStr == null || nodeToStr.equals("")) {
				nodeToStr = node.toString();
			}
		}
		return nodeToStr;
	}
	
}
