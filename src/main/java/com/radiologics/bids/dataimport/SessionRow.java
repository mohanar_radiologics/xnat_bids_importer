package com.radiologics.bids.dataimport;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Properties;

import org.nrg.xdat.bean.XnatExperimentdataFieldBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;

import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class SessionRow {
	Properties _properties = new Properties();

	public SessionRow(Properties properties) {
		_properties = properties;
	}
	
	public XnatMrsessiondataBean toXnatMrSessionDataBean() {
		XnatMrsessiondataBean session = new XnatMrsessiondataBean();
		String sessionLabel = _properties.getProperty(BIDSConstants.SESSION_ID);
		session.setLabel(sessionLabel);
		session.setVisitId(sessionLabel);
		try {
			Enumeration<?> e = _properties.keys();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				String value = _properties.getProperty(key);
				if (BIDSConstants.MRSESSION_FIELDS_MAP.containsKey(key) || BIDSConstants.MRSESSION_FIELDS_MAP.containsKey(key.toUpperCase())) {
					String methodName = "set" + key.toLowerCase().substring(0, 1).toUpperCase() + key.toLowerCase().substring(1);
				    Method setNameMethod = session.getClass().getMethod(methodName, String.class);
				    setNameMethod.invoke(session, value); // pass arg
				}else {
					XnatExperimentdataFieldBean expField = new XnatExperimentdataFieldBean();
					expField.setName(key);
					expField.setField(value);
					session.addFields_field(expField);
				}
			}
		}catch(Exception e) {
			
		}
		return session;
	}
}
