package com.radiologics.bids.dataimport.manager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.nrg.xdat.bean.XnatExperimentdataBean;
import org.nrg.xdat.bean.XnatImagescandataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.nrg.xdat.om.XnatSubjectdata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.radiologics.bids.manifest.ImportManifest;
import com.radiologics.bids.manifest.ImportedItem;
import com.radiologics.bids.manifest.ProjectImportedItem;
import com.radiologics.bids.manifest.SubjectImportItem;
import com.radiologics.bids.services.ImportManifestService;


/**
 * @author Mohana Ramaratnam
 *
 */
public class ImportManager {
    
	private static final Logger _log = LoggerFactory.getLogger(ImportManager.class);
	
	private static Map<String, Date> projectSyncStartTime = new HashMap<String,Date>();

	private static Map<String, ImportManifest> importManifests = new HashMap<String,ImportManifest>();
	private static String _manifest_log_files_root_path;
	private static Hashtable<String, Map<String,String>> projectSubjectXNATMap = new Hashtable<String, Map<String,String>>();
	private static Hashtable<String, Map<String,String>> projectExperimentXNATMap = new Hashtable<String, Map<String,String>>();
	
	public static void SAVE_SUBJECT_XNAT_ID(String project, String subjectLabel, String subjectId) {
		Map<String,String> subjectIds = projectSubjectXNATMap.get(project);
		if (subjectIds == null) {
			subjectIds = new HashMap<String, String>();
		}
		subjectIds.put(subjectLabel, subjectId);
	}
	

	public static void SAVE_EXPERIMENT_XNAT_ID(String project, String expLabel, String expId) {
		Map<String,String> experimentIds = projectExperimentXNATMap.get(project);
		if (experimentIds == null) {
			experimentIds = new HashMap<String, String>();
		}
		experimentIds.put(expLabel, expId);
	}

	public static String GET_SUBJECT_XNAT_ID(String project, String subjectLabel) {
		Map<String,String> subjectIds = projectSubjectXNATMap.get(project);
		String subjectId = null;
		if (subjectIds != null) {
			subjectId = subjectIds.get(subjectLabel);
		}
		return subjectId;
	}

/*	public static void SET_SUBJECT_XNAT_ID(String project, String subjectLabel, String subjectId) {
		Map<String,String> subjectIds = projectSubjectXNATMap.get(project);
		subjectIds.put(subjectLabel, subjectId);
	}
*/
	
	public static void SET_ROOT_FOLDER(String manifest_log_files_root_path) throws IOException {
		_manifest_log_files_root_path = manifest_log_files_root_path;
		
		File rootPath = new File(_manifest_log_files_root_path);
		if (!rootPath.exists()) {
			rootPath.mkdirs();
		}
		if (!rootPath.isDirectory())
			throw new IOException("Expecting a directory at this path " + _manifest_log_files_root_path);
		if (!_manifest_log_files_root_path.endsWith(File.separator)) {
			_manifest_log_files_root_path += File.separator;
		}
	}
	
	public static void BEGIN_SYNC(final ImportManifestService importManifestService, String projectId) {
		Date now = new Date();
		projectSyncStartTime.put(projectId, now);
		ImportManifest projectSyncManifest = new ImportManifest(importManifestService,  projectId);
		projectSyncManifest.setSync_start_time(now);
		importManifests.put(projectId, projectSyncManifest);
	}



	public synchronized static void UPDATE_MANIFEST(String projectId, ImportedItem item) {
		  ImportManifest manifest = importManifests.get(projectId);
		  if (manifest != null) {
			  if (manifest.getProjectId().equals(projectId)) {
					if (item instanceof SubjectImportItem) {
						manifest.addSubject((SubjectImportItem)item);
					}else if (item instanceof ProjectImportedItem) {
						System.out.println("Import Manifest UPDATE " + item.getLocalLabel());
						
					}
				}
		  }
	}

	public synchronized static void UPDATE_MANIFEST(String projectId, String message) {
		  ImportManifest manifest = importManifests.get(projectId);
		  if (manifest != null) {
			  if (manifest.getProjectId().equals(projectId)) {

			  }
		  }
	}

	
	public static void END_ERROR_FAILURE_SYNC(String projectId) {
	    ImportManifest manifest = importManifests.get(projectId);
	    if (manifest != null) {
			manifest.informUser();
			File syncInfoFilePath = new File(GET_SYNC_FILE_PATH(projectId)+projectId+"_import.html");
			manifest.syncInfoToFile(syncInfoFilePath);
	    }		
	}
	
	public static void END_SYNC( String projectId,  boolean save) {
		Date now = new Date();
//		projectSyncEndTime.put(projectId,now);
	    ImportManifest manifest = importManifests.get(projectId);
	    if (manifest != null) {
		  manifest.setSync_end_time(now);
			if (manifest.shouldNotify())
				manifest.informUser();
			File syncInfoFilePath = new File(GET_SYNC_FILE_PATH(projectId)+projectId+"_import.html");
			manifest.syncInfoToFile(syncInfoFilePath);
			//Clean up the cache path contents
			if (manifest.wasSyncSuccessfull()) cleanUp(projectId);
	    }
	}
	
	public static ImportManifest getProjectManifest(String projectId) {
	    ImportManifest manifest = importManifests.get(projectId);
	    return manifest;
	}
	
	private static void cleanUp(String projectId) {
		File folder = new File(GET_SYNC_FILE_PATH(projectId));
		if (folder.exists())
			rmdir(folder);
	}
	
	private static void rmdir(File folder) {
		  // check if folder file is a real folder
	      if (folder.isDirectory()) {
	          File[] list = folder.listFiles();
	          if (list != null) {
	              for (int i = 0; i < list.length; i++) {
	                  File tmpF = list[i];
	                  if (tmpF.isDirectory()) {
	                      rmdir(tmpF);
	                  }
	                  if (!tmpF.getAbsolutePath().endsWith("_import.html") && !tmpF.getAbsolutePath().endsWith("_import.log"))
	                	  tmpF.delete();
	              }
	          }
	      }
	}	

	private static String timeToPath(Date d) {
		SimpleDateFormat ft = 
			      new SimpleDateFormat ("yyyy.MM.dd'BIDSIMPORT'hh.mm.ss");
		if (d==null)
			d = new Date();
		return ft.format(d);
	}

	public static void SAVE_FAILED_UPLOAD(XnatProjectdataBean project) throws IOException{
		File projectXMLFilePath = new File(GET_SYNC_FAILURE_FILE_PATH(project.getId()) + project.getId()+"_failed.xml");
		FileWriter writer = new FileWriter(projectXMLFilePath);
		project.toXML(writer, true);
		writer.close();
	}

	public static void SAVE_FAILED_UPLOAD(XnatSubjectdataBean subject) throws IOException{
		File subjectXMLFilePath = new File(GET_SYNC_FAILURE_FILE_PATH(subject.getProject()) + subject.getLabel()+"_failed.xml");
		FileWriter writer = new FileWriter(subjectXMLFilePath);
		subject.toXML(writer, true);
		writer.close();
	}

	public static void SAVE_FAILED_UPLOAD(XnatExperimentdataBean experiment) throws IOException{
		File experimentXMLFilePath = new File(GET_SYNC_FAILURE_FILE_PATH(experiment.getProject()) + experiment.getLabel()+"_failed.xml");
		FileWriter writer = new FileWriter(experimentXMLFilePath);
		experiment.toXML(writer, true);
		writer.close();
	}

	public static String GET_SYNC_FILE_PATH(String projectId) {
		String syncPath = _manifest_log_files_root_path ;
		String timestampFolder = timeToPath(projectSyncStartTime.get(projectId));
		syncPath += "BIDS_DATA_IMPORT" + File.separator + projectId + File.separator +  timestampFolder + File.separator ; 
		return syncPath;
		
	}

	public static String GET_SYNC_LOG_FILE_PATH(String projectId) {
		String timestampFolder = timeToPath(projectSyncStartTime.get(projectId));
		return ImportManager.GET_SYNC_FILE_PATH(projectId)+projectId+"_"+timestampFolder+"_import.log";

	}

	
	public static String GET_SYNC_FILE_PATH(String projectId, XnatSubjectdata subject) {
		return GET_SYNC_FILE_PATH(projectId) +  "subjects" + File.separator ;
	}

	public static String GET_SYNC_FAILURE_FILE_PATH(String projectId) {
		String fPath =  GET_SYNC_FILE_PATH(projectId) +  "FAILURES" + File.separator ;
		File f  = new File(fPath);
		if (!f.exists())
			f.mkdirs();
		return fPath;
	}


	

}
