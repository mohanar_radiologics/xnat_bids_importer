/*
 * xnat-template: org.nrg.xnat.plugins.template.plugin.XnatTemplatePlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package com.radiologics.bids.importer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionHandler;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.dataimport.BIDSRootFolder;
import com.radiologics.bids.dataimport.manager.ImportManager;
import com.radiologics.bids.services.ImportStatusService;


@Service
public class BidsDataImporter {
	
        private static final Logger _log = LoggerFactory.getLogger(BidsDataImporter.class);
        private final RemoteConnectionManager       _manager;
	    private final ImportStatusService _importStatusService;

	    @Autowired
	    public BidsDataImporter(final RemoteConnectionManager manager, final ImportStatusService importStatusService) {
	    	_manager = manager;
	    	_importStatusService = importStatusService;
	    }

	    
	    public void importData(String[] args) {
	    	if (args== null || args.length == 0) {
	    		System.out.println("BIDS Data Importer");
	    		System.out.println("This application imports data organized as BIDS format into XNAT");
	    		System.out.println("Command-line arguments: " );
	    		System.out.println("0: HOST URL");
	    		System.out.println("1: XNAT Username");
	    		System.out.println("2: XNAT Password");
	    		System.out.println("3: Path to BIDS data Root Folder");
	    		System.out.println("4: Path to BIDS data import manifest file");
	    		System.exit(1);
	    	}
	    	String host = args[0];
	    	String xnat_username = args[1];
	    	String xnat_passwrd = args[2];
	    	String path_to_bids_root_folder = args[3];
	    	String path_to_manifest_file = args[4];
	    	try {
	    		ImportManager.SET_ROOT_FOLDER(path_to_manifest_file);
	    	}catch(IOException ioe) {
	    		System.out.println(ioe.getMessage());
	    		System.exit(1);
	    	}
	    	RemoteConnectionHandler remoteConnectionHandler = RemoteConnectionHandler.GetInstance(host, xnat_username, xnat_passwrd);
	    	RemoteConnection connection = remoteConnectionHandler.getRemoteConnection();
	    	BIDSRootFolder bidsRootFolder = new BIDSRootFolder(_manager,_importStatusService,connection,  path_to_bids_root_folder);
	    	bidsRootFolder.importData();
	    	System.out.println("Log files generated in  " + path_to_manifest_file );
	    }
	    


}
