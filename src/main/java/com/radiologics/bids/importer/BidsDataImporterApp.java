package com.radiologics.bids.importer;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

//import com.google.common.util.concurrent.Service;
import com.radiologics.bids.configuration.BidsDataImporterConfiguration;

/**
 * @author Mohana Ramaratnam
 *
 */
public class BidsDataImporterApp {
	
    public static void main(String[] args) {

    	AbstractApplicationContext context = new AnnotationConfigApplicationContext(BidsDataImporterConfiguration.class);
        BidsDataImporter p = context.getBean(BidsDataImporter.class);
        p.importData(args);
        context.close();
    	System.exit(0);

    }


}
