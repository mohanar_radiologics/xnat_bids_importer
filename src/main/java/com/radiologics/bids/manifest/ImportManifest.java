package com.radiologics.bids.manifest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.security.UserI;

import com.radiologics.bids.services.ImportManifestService;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ImportManifest{
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(ImportManifest.class);

	String projectId;
	Date sync_start_time;
	Date sync_end_time;
	String message;
	
	ArrayList<SubjectImportItem> subjects;


	private final ImportManifestService _syncManifestService;

	public ImportManifest(final ImportManifestService syncManifestService, String projectId) {
		_syncManifestService = syncManifestService;
		this.projectId = projectId;
	}


	/**
	 * @return the subjects
	 */
	public ArrayList<SubjectImportItem> getSubjects() {
		return subjects;
	}

	/**
	 * @param subjects the subjects to set
	 */
	public void setSubjects(ArrayList<SubjectImportItem> subjects) {
		this.subjects = subjects;
	}

	public void addSubject(SubjectImportItem subject) {
		subjects.add(subject);
	}



	/**
	 * @return the sync_start_time
	 */
	public Date getSync_start_time() {
		return sync_start_time;
	}

	/**
	 * @param sync_start_time the sync_start_time to set
	 */
	public void setSync_start_time(Date sync_start_time) {
		this.sync_start_time = sync_start_time;
	}

	/**
	 * @return the sync_end_time
	 */
	public Date getSync_end_time() {
		return sync_end_time;
	}

	/**
	 * @param sync_end_time the sync_end_time to set
	 */
	public void setSync_end_time(Date sync_end_time) {
		this.sync_end_time = sync_end_time;
	}

	

	/**
	 * @return the localProjectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @param message the message to append
	 */
	public void appendMessage(String message) {
		this.message += message;
	}


	public boolean wasSyncSuccessfull() {
		boolean wasSuccessful = true;
		return wasSuccessful;
	}
	
	



	public boolean shouldNotify() {
		boolean shouldNotify = false;
		return shouldNotify;	
	}
	
	public void informUser() {
		final Hashtable<String, String> info = syncInfoAsHTML();
	}

	/**
	 * Format sync information to requesting user.
	 *
	 */
	public Hashtable<String, String> syncInfoAsHTML() {
		final Hashtable<String,String> info = new Hashtable<>();
		final String subject="Project " + this.projectId ;
		info.put("SUBJECT", subject);
			StringBuilder sb = new StringBuilder();
			sb.append("<html>");
	        sb.append("<body>");
			sb.append("<p>The following data  was imported into project ").append(projectId).append(". </p>");


			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<th> Sync Start Time </th>");
			sb.append("<th> Sync End Time </th>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td>").append(this.getSync_start_time()).append("</td>");
			sb.append("<td>").append(this.getSync_end_time()).append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

			sb.append("<p>" + message + "</p>");
			sb.append("</body>");
            sb.append("</html>");
			logger.debug(sb.toString());
			info.put("BODY", sb.toString());
			return info;
		}
		/**
		 * Format sync information to requesting user.
		 *
		 */
		public void syncInfoToFile(File file) {
			final Hashtable<String, String> info = syncInfoAsHTML();

			file.getParentFile().mkdirs();
			try (final BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
		        writer.write(info.get("BODY"));
		    } catch (IOException e) {
				logger.error("An error occurred writing the sync file", e);
			}
		}

}
