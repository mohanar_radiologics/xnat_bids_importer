package com.radiologics.bids.manifest;

import java.util.Date;
import java.util.Observable;

import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public abstract class ImportedItem  extends Observable{
	String syncStatus;
	String localId;
	String localLabel;
	
	String xsiType;
	String message;
	Date syncTime;
	
	
	public ImportedItem(String localId, String localLabel) {
		super();
		this.localId = localId;
		this.localLabel = localLabel;
		message = "";
	}

	/**
	 * @return the localId
	 */
	public String getLocalId() {
		return localId;
	}

	/**
	 * @return the localId
	 */
	public void setLocalId(String l) {
		localId = l;
	}
	/**
	 * @return the localLabel
	 */
	public String getLocalLabel() {
		return localLabel;
	}

	/**
	 * @return the xsiType
	 */
	public String getXsiType() {
		return xsiType;
	}

	/**
	 * @param xsiType the xsiType to set
	 */
	public void setXsiType(String xsiType) {
		this.xsiType = xsiType;
	}

	/**
	 * @return the syncStatus
	 */
	public String getSyncStatus() {
		return syncStatus;
	}

	/**
	 * @param syncStatus the syncStatus to set
	 */
	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
		if (syncTime==null) {
			syncTime = new Date();
		}
	}




	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the syncTime
	 */
	public Date getSyncTime() {
		return syncTime;
	}

	/**
	 * @param syncTime the syncTime to set
	 */
	public void setSyncTime(Date syncTime) {
		this.syncTime = syncTime;
	}
	
	public String toString() {
		String str = "";
		final  String newline = BIDSConstants.NEWLINE;
		str += "XNAT ID:" + this.getLocalId() + newline;
		str += "Local Label:" + this.getLocalLabel() + newline;
		str += "XsiType:" + this.getXsiType() + newline;
		str += "Message: " + this.getMessage() + newline;
		str += "Sync Status:" + this.getSyncStatus() + newline;
		str += "Sync Time:" + this.getSyncTime() + newline;
		return str;
	}
	
	public void stateChanged() {
		setChanged();
		notifyObservers(this);
	}
	
	
}
