package com.radiologics.bids.manifest;

/**
 * @author Mohana Ramaratnam
 *
 */
public class ProjectImportedItem extends ImportedItem{

	public ProjectImportedItem(String localId, String localLabel) {
		super(localId, localLabel);
	}
}
