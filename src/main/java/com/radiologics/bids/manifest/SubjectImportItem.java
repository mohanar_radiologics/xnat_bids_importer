package com.radiologics.bids.manifest;

import com.radiologics.bids.utils.BIDSConstants;

/**
 * @author Mohana Ramaratnam
 *
 */
public class SubjectImportItem extends ImportedItem {
	
	
	public SubjectImportItem(String localId, String localLabel) {
		super(localId, localLabel);
	}
		
	
	

	public void updateSyncStatus(String status, String msg) {
		setMessage(msg);
		return;
	}	
	
	public String toString() {
		String str = super.toString();
		final  String newline = BIDSConstants.NEWLINE;
		return str;
	}
	

}
