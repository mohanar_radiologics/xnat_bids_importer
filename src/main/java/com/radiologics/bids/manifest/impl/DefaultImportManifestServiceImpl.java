package com.radiologics.bids.manifest.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.radiologics.bids.manifest.ImportManifest;
import com.radiologics.bids.services.ImportManifestService;

/**
 * @author Mohana Ramaratnam
 *
 */
@Service("importManifestService")
@JsonIgnoreProperties(value = { "created" })

public class DefaultImportManifestServiceImpl implements ImportManifestService{

	/* (non-Javadoc)
	 * @see com.radiologics.bids.services.ImportManifestService#persistHistory(com.radiologics.bids.manifest.ImportManifest)
	 */

	 @Transactional
	 public synchronized void persistHistory(ImportManifest manifest) {
		// TODO Auto-generated method stub
		System.out.println("Will save information");
	}

}
