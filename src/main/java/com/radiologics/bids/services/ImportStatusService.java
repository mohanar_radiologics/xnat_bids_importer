package com.radiologics.bids.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.radiologics.bids.components.ImportStatusHolder;
import com.radiologics.bids.components.ImportStatusHolder.SyncType;
import com.radiologics.bids.components.elements.ProjectImportStatus;
import com.radiologics.bids.manifest.ImportManifest;

@Service
public class ImportStatusService {
	
	ImportStatusHolder _statusHolder;
	private ImportManifestService _manifestService;
	
	@Autowired
	public ImportStatusService(ImportStatusHolder statusHolder, ImportManifestService manifestService) {
		_statusHolder = statusHolder;
		_manifestService = manifestService;
	}
	
	public void registerSyncStart(String projectId, SyncType syncType, ImportManifest syncManifest) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.setSyncing(true);
		if (syncManifest!=null) {
			projectSyncStatus.setSyncStartTime(syncManifest.getSync_start_time());
		} else {
			projectSyncStatus.setSyncStartTime(new Date());
		}
		projectSyncStatus.setSyncEndTime(null);
		projectSyncStatus.setSyncType(syncType);
		projectSyncStatus.setCurrentSubject(null);
		projectSyncStatus.setCurrentExperiment(null);
		projectSyncStatus.setCurrentExperimentType(null);
		projectSyncStatus.getInitialSubjectList().clear();
		projectSyncStatus.getCompletedSubjects().clear();
		projectSyncStatus.getFailedSubjects().clear();
		projectSyncStatus.getCompletedExperiments().clear();
		projectSyncStatus.getFailedExperiments().clear();
	}

	public void registerSyncEnd(String projectId, ImportManifest syncManifest) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.setSyncing(false);
		if (syncManifest!=null) {
			projectSyncStatus.setWasSyncSuccessful(syncManifest.wasSyncSuccessfull());
			projectSyncStatus.setSyncEndTime(syncManifest.getSync_end_time());
		} else {
			projectSyncStatus.setSyncEndTime(new Date());
		}
		projectSyncStatus.setCurrentSubject(null);
		projectSyncStatus.setCurrentExperiment(null);
	}
	
	public void registerCurrentSubject(String projectId, String subjectId) {
		_statusHolder.getProjectImportStatus(projectId).setCurrentSubject(subjectId);
	}
	
	public void registerCurrentExperiment(String projectId, String expId, String expType) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.setCurrentExperiment(expId);
		projectSyncStatus.setCurrentExperimentType(expType);
	}
	
	public void registerCompletedSubject(String projectId, String subjectId) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.getCompletedSubjects().add(subjectId);
		projectSyncStatus.setCurrentSubject(null);
	}

	public void registerFailedSubject(String projectId, String subjectId) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.getFailedSubjects().add(subjectId);
		projectSyncStatus.setCurrentSubject(null);
	}
	
	public void registerCompletedExperiment(String projectId, String expId, String expType) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.getCompletedExperiments().put(expId,  expType);
		projectSyncStatus.setCurrentExperiment(null);
		projectSyncStatus.setCurrentExperimentType(null);
	}

	public void registerFailedExperiment(String projectId, String expId, String expType) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.getFailedExperiments().put(expId, expType);
		projectSyncStatus.setCurrentExperiment(null);
		projectSyncStatus.setCurrentExperimentType(null);
	}
	
	public boolean isCurrentlySyncing(String projectId) {
		return _statusHolder.getProjectImportStatus(projectId).isSyncing();
	}
	
	public ProjectImportStatus getProjectSyncStatus(String projectId) {
		return _statusHolder.getProjectImportStatus(projectId);
	}

	public void registerInitialSubjectList(String projectId, List<String> subjectIds) {
		final ProjectImportStatus projectSyncStatus = _statusHolder.getProjectImportStatus(projectId);
		projectSyncStatus.getInitialSubjectList().clear();
		projectSyncStatus.getInitialSubjectList().addAll(subjectIds);
		
	}

}
