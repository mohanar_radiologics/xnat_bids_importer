package com.radiologics.bids.services;

import java.io.File;

import org.nrg.xdat.bean.XnatExperimentdataBean;
import org.nrg.xdat.bean.XnatImagescandataBean;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;

import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionResponse;

/**
 * @author Mohana Ramaratnam
 *
 */
public interface RemoteRESTService {
	public RemoteConnectionResponse importProject(RemoteConnection connection, XnatProjectdataBean project) throws Exception;

	public RemoteConnectionResponse importProjectResource(RemoteConnection connection, String projectId, String resourceLabel, File zipFile) throws Exception;
	
	public RemoteConnectionResponse importSubject(RemoteConnection connection, XnatSubjectdataBean subject) throws Exception;
	public RemoteConnectionResponse importImageSession(RemoteConnection connection, XnatImagesessiondataBean experiment) throws Exception;

	public RemoteConnectionResponse importResource(RemoteConnection connection, String url_postfix, File zipFile) throws Exception;


    public RemoteConnectionResponse getResult(RemoteConnection connection, String uri) throws Exception;

}
