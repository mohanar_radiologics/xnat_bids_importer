package com.radiologics.bids.services;


import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.nrg.xdat.bean.XnatExperimentdataBean;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.radiologics.bids.connection.RadiologicsHttpAuthenticationException;
import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.connection.RemoteConnectionResponse;
import com.radiologics.bids.dataimport.manager.ImportManager;


/**
 * The Class RemoteRESTServiceImpl.
 */
@Service("remoteRESTService")
//@EnableRetry
//TODO update to retry when we upgrade spring to 4
public class RemoteRESTServiceImpl  extends AbstractRemoteRESTService implements RemoteRESTService {

	/** The logger. */
	public static Logger logger = Logger.getLogger(RemoteRESTServiceImpl.class);
	// TODO:  Do we want this to be configurable?
	public static final int TRUNCATE_LOG_OUTPUT_LENGTH = 1000;

	private long sleep = 10;
	private int maxTries = 5;


	@Autowired
	public RemoteRESTServiceImpl() {}


	
	

	/**
	 * Import Subject with retry.
	 *
	 * @param connection the connection
	 * @param subject the subject
	 * @return response
	 */
	public RemoteConnectionResponse importProject(RemoteConnection connection,XnatProjectdataBean project ) throws Exception{
		int count = 0;
		while(true) {
		    try {
		         return this.importProjectWithoutRetry( connection, project );
		    } catch (RuntimeException e) {
		    	try {
		    		e.printStackTrace();
		    		logger.debug("Exception " + e.getMessage());
			    	if (maxTries > 0) {
				    	logger.error("importProject: retrycount "+ count);
				    	logger.error("Referesh rate is " + maxTries);
				    	logger.error("Referesh rate is " + sleep);
				    	logger.error("Sleeping for " + sleep + " milliseconds");
			    		Thread.sleep(sleep);
			    	}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        // handle exception
		        if (maxTries == 0 || ++count == maxTries) throw e;
		    }
		}
	}
	
	/**
	 * Import project without retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param project the project
	 * @return true, if successful
	 */
	//TODO @Retryable(maxAttempts=5) update to retry when we upgrade spring to 4
	private RemoteConnectionResponse importProjectWithoutRetry(RemoteConnection connection,XnatProjectdataBean project ) throws Exception{
		//do we need the assessor data and how.
		//MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();     
		
		// NOTE: Just call toXML on the subject object here, rather than calling getItem on the subject object and obtaining
		// XML from the item.  Using getItem() will query for experiments and assessors associated with the assession number.
		// In the case where there is a source-side subject with the same assession number as the destination-side subject,
		// this could result in the wrong sessions being sent to the destination, potentially overwriting previously synced
		// sessions with sessions from a subject outside the source project.
		final StringWriter tsw = new StringWriter();
		project.toXML(tsw);
		tsw.close();
		final String projectXml = tsw.toString();
		
		ResponseEntity<String> response;
		try {
			logger.debug("URL: " + connection.getUrl()+"/data/archive/projects/"+project.getId()+"?inbody=true");
			final HttpEntity<?> httpEntity = new HttpEntity<String>(projectXml, RemoteConnectionManager.GetAuthHeaders(connection, true));
			response = getResttemplate().exchange(connection.getUrl()+"/data/archive/projects/"+project.getId()+"?inbody=true", HttpMethod.PUT, httpEntity, String.class);
			logger.debug(response);
		}catch (RadiologicsHttpAuthenticationException authex) {
			try {
				final HttpEntity<?> httpEntity = new HttpEntity<String>(projectXml, RemoteConnectionManager.GetAuthHeaders(connection, false, true));
				response = getResttemplate().exchange(connection.getUrl()+"/data/archive/projects/"+project.getId()+"?inbody=true", HttpMethod.PUT, httpEntity, String.class);
				logger.debug(response);
			}catch(Exception e) {
				logger.debug("Error while storing project " + e.getMessage());
				ImportManager.SAVE_FAILED_UPLOAD(project);
				throw e;
			}
		}
		logger.debug(response);
		//return 	((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;
		return new RemoteConnectionResponse(response);
	}

	/**
	 * Import ImageSession with retry.
	 *
	 * @param connection the connection
	 * @param experiment the experiment
	 * @return response
	 */
	public RemoteConnectionResponse importImageSession(RemoteConnection connection,XnatImagesessiondataBean experiment ) throws Exception{
		int count = 0;
		while(true) {
		    try {
		         return this.importImageSessionWithoutRetry( connection, experiment );
		    } catch (RuntimeException e) {
		    	try {
		    		e.printStackTrace();
		    		logger.debug("Exception " + e.getMessage());
			    	if (maxTries > 0) {
				    	logger.error("importProject: retrycount "+ count);
				    	logger.error("Referesh rate is " + maxTries);
				    	logger.error("Referesh rate is " + sleep);
				    	logger.error("Sleeping for " + sleep + " milliseconds");
			    		Thread.sleep(sleep);
			    	}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        // handle exception
		        if (maxTries == 0 || ++count == maxTries) throw e;
		    }
		}
	}

	/**
	 * Import Experiment without retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param experiment the experiment
	 * @return true, if successful
	 */
	//TODO @Retryable(maxAttempts=5) update to retry when we upgrade spring to 4
	private RemoteConnectionResponse importImageSessionWithoutRetry(RemoteConnection connection,XnatImagesessiondataBean experiment ) throws Exception{
		//do we need the assessor data and how.
		//MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();     
		
		// NOTE: Just call toXML on the subject object here, rather than calling getItem on the subject object and obtaining
		// XML from the item.  Using getItem() will query for experiments and assessors associated with the assession number.
		// In the case where there is a source-side subject with the same assession number as the destination-side subject,
		// this could result in the wrong sessions being sent to the destination, potentially overwriting previously synced
		// sessions with sessions from a subject outside the source project.
		final StringWriter tsw = new StringWriter();
		experiment.toXML(tsw);
		tsw.close();
		final String experimentXml = tsw.toString();
		
		ResponseEntity<String> response;
		String url = connection.getUrl()+"/data/archive/projects/"+experiment.getProject()+"/subjects/"+experiment.getSubjectId()+"/experiments/"+experiment.getLabel()+"?inbody=true";

		try {
			logger.debug("URL: " + url);
			final HttpEntity<?> httpEntity = new HttpEntity<String>(experimentXml, RemoteConnectionManager.GetAuthHeaders(connection, true));
			response = getResttemplate().exchange(url, HttpMethod.PUT, httpEntity, String.class);
			logger.debug(response);
		}catch (RadiologicsHttpAuthenticationException authex) {
			try {
				final HttpEntity<?> httpEntity = new HttpEntity<String>(experimentXml, RemoteConnectionManager.GetAuthHeaders(connection, false, true));
				response = getResttemplate().exchange(url, HttpMethod.PUT, httpEntity, String.class);
				logger.debug(response);
			}catch(Exception e) {
				logger.debug("Error while storing project " + e.getMessage());
				ImportManager.SAVE_FAILED_UPLOAD(experiment);
				throw e;
			}
		}
		logger.debug(response);
		//return 	((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;
		return new RemoteConnectionResponse(response);
	}

	
	/**
	 * Import Subject with retry.
	 *
	 * @param connection the connection
	 * @param subject the subject
	 * @return response
	 */
	public RemoteConnectionResponse importSubject(RemoteConnection connection,XnatSubjectdataBean subject ) throws Exception{
		int count = 0;
		while(true) {
		    try {
		         return this.importSubjectWithoutRetry( connection, subject );
		    } catch (RuntimeException e) {
		    	try {
		    		e.printStackTrace();
		    		logger.debug("Exception " + e.getMessage());
			    	if (maxTries > 0) {
				    	logger.error("importSubject: retrycount "+ count);
				    	logger.error("Referesh rate is " + maxTries);
				    	logger.error("Referesh rate is " + sleep);
				    	logger.error("Sleeping for " + sleep + " milliseconds");
			    		Thread.sleep(sleep);
			    	}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        // handle exception
		        if (maxTries == 0 || ++count == maxTries) throw e;
		    }
		}
	}

	
	/**
	 * import Subject Resource with retry.
	 *
	 * @param connection the connection
	 * @param subjectId the subject ID
	 * @return response
	 */
	public RemoteConnectionResponse importSubjectResource(RemoteConnection connection,XnatSubjectdataBean subject, String resourceLabel, File zipFile ){
		int count = 0;
		while(true) {
		    try {
		    	 String uri = connection.getUrl()+"/data/archive/projects/"+subject.getProject()+"/subjects/"+subject.getId()+"/resources/"+ resourceLabel ;
		         if (zipFile != null) {
			    	uri += "/files?overwrite=true&extract=true";
		         }
		    	 return this.importZipWithoutRetry( connection, uri, zipFile);
		    } catch (RuntimeException e) {
		    	try {
			    	logger.error("importsubjectresource: retrycount "+ count);
					if (maxTries > 0) Thread.sleep(sleep);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        // handle exception
		        if (maxTries == 0 || ++count == maxTries) throw e;
		    }
		}
	}

	/**
	 * import Project Resource with retry.
	 *
	 * @param connection the connection
	 * @param projectId the Project ID
	 * @return response
	 */
	public RemoteConnectionResponse importProjectResource(RemoteConnection connection,String projectId, String resourceLabel, File zipFile ) throws Exception{
		int count = 0;
		while(true) {
		    try {
		    	 String uri = connection.getUrl()+"/data/archive/projects/"+projectId+"/resources/"+ resourceLabel +"/files?overwrite=true&extract=true";
		         return this.importZipWithoutRetry( connection, uri, zipFile);
		    } catch (RuntimeException e) {
		    	try {
			    	logger.error("importsubjectresource: retrycount "+ count);
					if (maxTries > 0) Thread.sleep(sleep);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        // handle exception
		        if (maxTries == 0 || ++count == maxTries) throw e;
		    }
		}
	}

	
	/**
	 * import ImageScan Resource with retry.
	 *
	 * @param connection the connection
	 * @param experiment the experiment
	 * @return response
	 */
	public RemoteConnectionResponse importResource(RemoteConnection connection,String url_postfix,  File zipFile ) throws Exception{
		int count = 0;
		while(true) {
		    try {
		    	 String uri = connection.getUrl() + url_postfix;
		         return this.importZipWithoutRetry( connection, uri, zipFile);
		    } catch (RuntimeException e) {
		    	try {
			    	logger.error("importImagescanresource: retrycount "+ count);
					if (maxTries > 0) Thread.sleep(sleep);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        // handle exception
		        if (maxTries == 0 || ++count == maxTries) throw e;
		    }
		}
	}
	
	/**
	 * Import subject without retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param subject the subject
	 * @return true, if successful
	 */
	//TODO @Retryable(maxAttempts=5) update to retry when we upgrade spring to 4
	private RemoteConnectionResponse importSubjectWithoutRetry(RemoteConnection connection,XnatSubjectdataBean subject ) throws Exception{
		//do we need the assessor data and how.
		//MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();     
		
		// NOTE: Just call toXML on the subject object here, rather than calling getItem on the subject object and obtaining
		// XML from the item.  Using getItem() will query for experiments and assessors associated with the assession number.
		// In the case where there is a source-side subject with the same assession number as the destination-side subject,
		// this could result in the wrong sessions being sent to the destination, potentially overwriting previously synced
		// sessions with sessions from a subject outside the source project.
		final StringWriter tsw = new StringWriter();
		subject.toXML(tsw);
		tsw.close();
		final String subjectXml = tsw.toString();
		
		ResponseEntity<String> response;
		try {
			logger.debug("URL: " + connection.getUrl()+"/data/archive/projects/"+subject.getProject()+"/subjects/"+subject.getLabel()+"?inbody=true");
			final HttpEntity<?> httpEntity = new HttpEntity<String>(subjectXml, RemoteConnectionManager.GetAuthHeaders(connection, true));
			response = getResttemplate().exchange(connection.getUrl()+"/data/archive/projects/"+subject.getProject()+"/subjects/"+subject.getLabel()+"?inbody=true", HttpMethod.PUT, httpEntity, String.class);
			logger.debug(response);
		} catch (RadiologicsHttpAuthenticationException authex) {
			try {
				final HttpEntity<?> httpEntity = new HttpEntity<String>(subjectXml, RemoteConnectionManager.GetAuthHeaders(connection, false, true));
				response = getResttemplate().exchange(connection.getUrl()+"/data/archive/projects/"+subject.getProject()+"/subjects/"+subject.getLabel()+"?inbody=true", HttpMethod.PUT, httpEntity, String.class);
				logger.debug(response);
			}catch(Exception e) {
				logger.debug("Error while storing subject " + e.getMessage());
				String cachePath = ImportManager.GET_SYNC_FILE_PATH(subject.getProject());
				File subjectF = new File(cachePath + "failed_" + subject.getLabel()+".xml");
				if (!subjectF.getParentFile().exists())
					subjectF.getParentFile().mkdirs();
				FileWriter fw = new FileWriter(subjectF);
				subject.toXML(fw, false);
				fw.close();
				throw e;
			}
		}
		
		logger.debug(response);
		//return 	((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;
		return new RemoteConnectionResponse(response);
	}

	

	/**
	 * Import Zip without retry.
	 *
	 * @param connection the connection
	 * @param zip the zip
	 * @return true, if successful
	 * @throws RuntimeException the runtime exception
	 */
	//@Retryable(maxAttempts=5,value=RuntimeException.class,backoff= @Backoff(delay=100, maxDelay=500))
	//TODO update to retry when we upgrade spring to 4
	private RemoteConnectionResponse importZipWithoutRetry(RemoteConnection connection,  String uri, File zip) throws RuntimeException{
		//this.setAliasToken(connection);
		
		final MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();     
		body.add("field", "value");
		if (zip != null) body.add("file", new FileSystemResource(zip));
		
		ResponseEntity<String> response;
		try {
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(body, RemoteConnectionManager.GetAuthHeaders(connection, true));
			response = getResttemplate().exchange(uri, HttpMethod.PUT, httpEntity, String.class);
		} catch (RadiologicsHttpAuthenticationException authex) {
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(body, RemoteConnectionManager.GetAuthHeaders(connection, false, true));
			response = getResttemplate().exchange(uri, HttpMethod.PUT, httpEntity, String.class);
		}
		logger.info(truncateStr(response));
		logger.info(truncateStr(response.getBody()));
		logger.info(truncateStr(response.getHeaders().get("Set-Cookie")));
		boolean status= ((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;
		logger.warn("importZip"+zip.getName());
		if(!status){
			throw new RuntimeException("importZip request failed. Retrying...");
		}else{
			return new RemoteConnectionResponse(response);
		}
	}


	
	/**
	 * Get URI result.
	 *
	 * @param connection the connection
	 * @param uri the uri
	 * @return ResponseEntity wrapper
	 */
	public RemoteConnectionResponse getResult(RemoteConnection connection,String uri) throws Exception{
		ResponseEntity<String> response;
		try {
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(RemoteConnectionManager.GetAuthHeaders(connection, true));
			response = getResttemplate().exchange(uri, HttpMethod.GET, httpEntity, String.class);
		} catch (RadiologicsHttpAuthenticationException e) {
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(RemoteConnectionManager.GetAuthHeaders(connection, false, true));
			response = getResttemplate().exchange(uri, HttpMethod.GET, httpEntity, String.class);
		}
		logger.info(truncateStr(response));
		logger.info(truncateStr(response.getBody()));
		logger.info(truncateStr(response.getHeaders().get("Set-Cookie")));
		return new RemoteConnectionResponse(response);
	}
	
	public String truncateStr(Object obj) {
		return truncateStr(obj,TRUNCATE_LOG_OUTPUT_LENGTH);
	}
	
	public String truncateStr(Object obj, int maxlength) {
		if (obj==null) {
			return null;
		}
		return (obj.toString().length()>TRUNCATE_LOG_OUTPUT_LENGTH) ? obj.toString().substring(0,TRUNCATE_LOG_OUTPUT_LENGTH) + "......." : obj.toString();
	}
	
}
