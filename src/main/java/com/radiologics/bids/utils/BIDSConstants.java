package com.radiologics.bids.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * @author Mohana Ramaratnam
 *
 */
public class BIDSConstants {

	public static final String NEWLINE = System.getProperty("line.separator");

	public final static String PARTICIPANTS_TSV_FILENAME = "participants.tsv";
	public final static String PARTICIPANTS_JSON_FILENAME = "participants.json";

	public final static String DATASET_DESCRIPTION_JSON = "dataset_description.json";
	public final static String README = "README";
	public final static String CHANGES = "CHANGES";
	public final static String DERIVATIVES_FOLDER_NAME  = "derivatives";
	
	public final static  String SUBJECT_PREFIX = "sub-";
	public final static  String SESSION_PREFIX = "ses-";
	public final static  String TASK_PREFIX = "task-";
	public final static  String ACQ_PREFIX = "acq-";
	public final static  String CE_PREFIX = "ce-";
	public final static  String REC_PREFIX = "rec-";
	public final static  String RUN_PREFIX = "run-";
	public final static  String ECHO_PREFIX = "echo-";
	public final static  String MOD_PREFIX = "mod-";
	public final static  String MODALITY_LABEL = "MODALITY_LABEL";
	public final static  String BOLD = "bold";
	public final static  String SBREF = "sbref";

	public final static  String JSON_EXT = ".json";
	public final static  String NII_EXT = ".nii";
	public final static  String NII_GZ_EXT = ".nii.gz";
	public final static  String TSV_EXT = ".tsv";
	

	
	
	public final static String PARTICIPANT_ID = "participant_id";
	public final static String SESSION_ID = "session_id";
	public final static String SITE = "site";
	
	public final static String NIFTI_RESOURCE = "NIFTI";
	public final static String BIDS_RESOURCE = "BIDS";
	public final static String DERIVATIVES_RESOURCE = "derivatives";
	
	
	
	//Subject LEVEL known demographics
	
	public static final Map<String,String> SubjectDemographicsMap = new  HashMap<String, String>();
	static {
		SubjectDemographicsMap.put("group","1");
		SubjectDemographicsMap.put("src","1");
		SubjectDemographicsMap.put("initials","1");
		//SubjectDemographicsMap.put("age","1");
		SubjectDemographicsMap.put("gender","1");
		SubjectDemographicsMap.put("handedness","1");
		SubjectDemographicsMap.put("education","1");
		SubjectDemographicsMap.put("educationDesc","1");
		SubjectDemographicsMap.put("race","1");
		SubjectDemographicsMap.put("race2","1");
		SubjectDemographicsMap.put("race3","1");
		SubjectDemographicsMap.put("race4","1");
		SubjectDemographicsMap.put("race5","1");
		SubjectDemographicsMap.put("race6","1");
		SubjectDemographicsMap.put("ethnicity","1");
		SubjectDemographicsMap.put("weight","1");
		SubjectDemographicsMap.put("height","1");
		SubjectDemographicsMap.put("ses","1");
		SubjectDemographicsMap.put("employment","1");
		SubjectDemographicsMap.put("gestational_age","1");
		SubjectDemographicsMap.put("post_menstrual_age","1");
		SubjectDemographicsMap.put("birth_weight","1");
	}

	public static final Map<String,String> MRSESSION_FIELDS_MAP = new  HashMap<String, String>();
	static {
		MRSESSION_FIELDS_MAP.put("date","1");
		MRSESSION_FIELDS_MAP.put("time","1");
		MRSESSION_FIELDS_MAP.put("duration","1");
		MRSESSION_FIELDS_MAP.put("delay","1");
		MRSESSION_FIELDS_MAP.put("note","1");

		MRSESSION_FIELDS_MAP.put("age","1");

		MRSESSION_FIELDS_MAP.put("scanner","1");
		MRSESSION_FIELDS_MAP.put("scanner_manufacturer","1");
		MRSESSION_FIELDS_MAP.put("scanner_model","1");

		MRSESSION_FIELDS_MAP.put("operator","1");

		MRSESSION_FIELDS_MAP.put("coil","1");
		MRSESSION_FIELDS_MAP.put("fieldStrength","1");
		MRSESSION_FIELDS_MAP.put("marker","1");
		MRSESSION_FIELDS_MAP.put("stabilization","1");
		MRSESSION_FIELDS_MAP.put("stabilization","1");
	}
	
	public static final ArrayList<String> DataDescriptionKeys = new ArrayList<String>();
    static {
    	DataDescriptionKeys.add("Name");
    	DataDescriptionKeys.add("BIDSVersion");
    	DataDescriptionKeys.add("License");
    	DataDescriptionKeys.add("Description");
    	DataDescriptionKeys.add("Authors");
    	DataDescriptionKeys.add("Funding");
    	DataDescriptionKeys.add("DatasetDOI");
    	DataDescriptionKeys.add("ReferencesAndLinks");
    }
    
	public static final ArrayList<String> BIDSJsonKeys = new ArrayList<String>();
    static {
    	BIDSJsonKeys.add("LongName");
    	BIDSJsonKeys.add("Description");
    	BIDSJsonKeys.add("Levels");
    	BIDSJsonKeys.add("Units");
    	BIDSJsonKeys.add("TermURL");
     }

	public static final Hashtable<String,String> ANATOMICAL_MODALITIES = new Hashtable<String, String>();
    static {
    	ANATOMICAL_MODALITIES.put("T1w", "T1 weighted");
    	ANATOMICAL_MODALITIES.put("T2w", "T2 weighted");
    	ANATOMICAL_MODALITIES.put("T1rho", "T1 Rho map");
    	ANATOMICAL_MODALITIES.put("T1map", "T1 map");
    	ANATOMICAL_MODALITIES.put("T2map", "T2 map");
    	ANATOMICAL_MODALITIES.put("T2Star", "T2star");
    	ANATOMICAL_MODALITIES.put("FLAIR", "FLAIR");
    	ANATOMICAL_MODALITIES.put("FLASH", "FLASH");
    	ANATOMICAL_MODALITIES.put("Proton density", "PD");
       	ANATOMICAL_MODALITIES.put("Proton density map", "PDmap");
       	ANATOMICAL_MODALITIES.put("Combined PD/T2", "PDT2");
       	ANATOMICAL_MODALITIES.put("Inplane T1", "inplaneT1");
      	ANATOMICAL_MODALITIES.put("Inplane T2", "inplaneT2");
      	ANATOMICAL_MODALITIES.put("Angiography", "angio");
    	ANATOMICAL_MODALITIES.put("Defacing mask", "defacenask");   	
       	ANATOMICAL_MODALITIES.put("Susceptibility Weighted Imaging(SWI)", "SWImagandphase");   	
    }

    
	public static final Hashtable<String,String> BIDS_TO_XNAT_JSON_ELEMENT_MAPPING = new Hashtable<String, String>();
    static {
    	BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.put("EchoTime", "Parameters_te");
    	BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.put("InversionTime", "Parameters_ti");
    	BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.put("SliceEncodingDirection", "Parameters_phaseencodingdirection");
    	BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.put("FlipAngle", "Parameters_flip");
    	BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.put("NumberOfSlices", "Frames");
    	BIDS_TO_XNAT_JSON_ELEMENT_MAPPING.put("RepetitionTime", "Parameters_tr");

    }

    public static final List<String>  DATATYPE_FOLDERS = Arrays.asList("anat", "func", "dwi", "fmap");
	
}
