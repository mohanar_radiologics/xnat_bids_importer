package com.radiologics.bids.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.radiologics.bids.dataimport.manager.ImportManager;

/**
 * @author Mohana Ramaratnam
 *
 */
public class BIDSFileUtils {

	
	public static boolean representsBIDSDataset(File bidsRootDir) {
		boolean isBIDSDatasetDir = false;
		File[] jsonFiles = bidsRootDir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.equals(BIDSConstants.DATASET_DESCRIPTION_JSON);
		    }
		});
		
		if (jsonFiles != null && jsonFiles.length == 1) {
			isBIDSDatasetDir = true;
		}
		return isBIDSDatasetDir;
	}
	

}
