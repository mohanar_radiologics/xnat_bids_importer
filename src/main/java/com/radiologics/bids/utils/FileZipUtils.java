package com.radiologics.bids.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.radiologics.bids.dataimport.manager.ImportManager;

/**
 * @author Mohana Ramaratnam
 *
 */
public class FileZipUtils {
	
	public FileZipUtils() {
		
	}
	
	public  File buildZip(String remoteProjectId,List<File> filesInZip, File rootDir) throws Exception {
		File zipFile = null;
		try {
			String expCachePath = ImportManager.GET_SYNC_FILE_PATH(remoteProjectId);
			new File(expCachePath).mkdirs();
			if (filesInZip != null && filesInZip.size() > 0) {
				zipFile = new File(expCachePath, (new Date()).getTime()+".zip");
				createZipFile(zipFile,filesInZip, rootDir);
				zipFile.deleteOnExit();
			}
		} catch (Exception e) {
			throw new Exception("Unable to create/save zip file "+e.getMessage());
		}
		return zipFile;
	}

	//Parse the list of files and separate the NIFTI and the other files
	//Put NIFTI in NIFTI resource and others in  BIDS resource
	public  Hashtable<String, File> buildZipByCategory(String remoteProjectId,Hashtable<String, List<File>> filesInZip, File rootDir) throws Exception {
		 Hashtable<String, File> zipFilesByCategory = new Hashtable<String, File>();
		try {
			String expCachePath = ImportManager.GET_SYNC_FILE_PATH(remoteProjectId);
			new File(expCachePath).mkdirs();
			if (filesInZip != null && filesInZip.size() > 0 ) {
	
				Enumeration<String> categoryIterator = filesInZip.keys();
				while (categoryIterator.hasMoreElements()) {
					String fileCategory = categoryIterator.nextElement();
					List<File> filesInCategory = filesInZip.get(fileCategory);
					if (filesInCategory != null && filesInCategory.size() > 0) {
						File zipFile = new File(expCachePath, fileCategory + "_" +(new Date()).getTime()+".zip");
						createZipFile(zipFile,filesInCategory, rootDir);
						zipFile.deleteOnExit();
						if (zipFile != null) {
							zipFilesByCategory.put(fileCategory, zipFile);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new Exception("Unable to create/save zip file "+e.getMessage());
		}
		return zipFilesByCategory;
	}

	
	
	
	public  void createZipFile(File zipFile, List<File> srcFiles, File rootDir) throws Exception{
		try {
			// create byte buffer
			byte[] buffer = new byte[1024];
			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);
			for (File f: srcFiles) {
				if (f!=null && f.isFile()) {
					FileInputStream fis = new FileInputStream(f);
					// begin writing a new ZIP entry, positions the stream to the start of the entry data
					zos.putNextEntry(new ZipEntry(f.getName()));
					int length;
					while ((length = fis.read(buffer)) > 0) {
						zos.write(buffer, 0, length);
					}
					zos.closeEntry();
					fis.close();
				}else {
					//Directory relative path needs to be preserved
					URI base = rootDir.toURI();
				    Deque<File> queue = new LinkedList<File>();
				    queue.push(f);
				      while (!queue.isEmpty()) {
				        File directory = queue.pop();
				        for (File kid : directory.listFiles()) {
				          String name = base.relativize(kid.toURI()).getPath();
				          if (kid != null && kid.isDirectory()) {
				            queue.push(kid);
				            name = name.endsWith("/") ? name : name + "/";
				            zos.putNextEntry(new ZipEntry(name));
				          } else {
				            zos.putNextEntry(new ZipEntry(name));
							FileInputStream fis = new FileInputStream(kid);
				            int length;
							while ((length = fis.read(buffer)) > 0) {
								zos.write(buffer, 0, length);
							}
				            zos.closeEntry();
				            fis.close();
				          }
				        }
				      }
				 }
			}
			// close the ZipOutputStream
			zos.close();
		}
		catch (IOException ioe) {
			System.out.println("Error creating zip file: " + ioe);
			throw ioe;
		}		
	}

	
	
	public static void getAllFiles(File dir, List<File> fileList) {
			File[] files = dir.listFiles();
			for (File file : files) {
				fileList.add(file);
			}
	}


	public static void addToZip(File directoryToZip, File file, ZipOutputStream zos) throws FileNotFoundException, IOException {

		FileInputStream fis = new FileInputStream(file);

		// we want the zipEntry's path to be a relative path that is relative
		// to the directory being zipped, so chop off the rest of the path
		String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1,
				file.getCanonicalPath().length());
		ZipEntry zipEntry = new ZipEntry(zipFilePath);
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
	}

}

